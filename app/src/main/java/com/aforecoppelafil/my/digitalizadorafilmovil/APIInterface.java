package com.aforecoppelafil.my.digitalizadorafilmovil;

import com.aforecoppelafil.my.digitalizadorafilmovil.pojo.DoctosCompDom;
import com.aforecoppelafil.my.digitalizadorafilmovil.pojo.DoctosIndexados;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.Headers;
import retrofit2.http.POST;

/**
 * Created by Sergio Gil (AP Interfaces) on 02/05/19.
 */

interface APIInterface {

    @Headers({
            "Authorization:Basic YWRtaW46MTIzNA==",
            "x-api-key:1234",
            "Content-Type:application/x-www-form-urlencoded"
    })
    @FormUrlEncoded
    @POST("ObtenerDoctoIndexado")
    Call<DoctosIndexados> doGetListResourcesIndex(@Field("info") String info);

    @Headers({
            "Authorization:Basic YWRtaW46MTIzNA==",
            "x-api-key:1234",
            "Content-Type:application/x-www-form-urlencoded"
    })
    @FormUrlEncoded
    @POST("obtenertipocomprobante")
    Call<DoctosCompDom> doGetListCompDom(@Field("info") String info);

}
