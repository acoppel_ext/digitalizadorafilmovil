package com.aforecoppelafil.my.digitalizadorafilmovil.documento;

import android.os.Parcel;
import android.os.Parcelable;

import com.aforecoppelafil.my.digitalizadorafilmovil.R;

public class Documento implements Parcelable{
    private int icono;
    private int identificador;
    private String tipoDocto;
    private String nomenclatura;
    private String instruccion;
    private boolean bProcesoTerminado;
    int posicion;

    public Documento(String tipoDocto, int identificador, String nomenclatura) {
        this.tipoDocto = tipoDocto;
        this.identificador = identificador;
        this.nomenclatura = nomenclatura;
        this.instruccion = "";
        this.bProcesoTerminado = false;
        this.icono = R.drawable.ic_file;
    }

    public Documento(){
        this.tipoDocto = "Incorrecto";
        this.identificador = -100;
        this.nomenclatura = "XXXX";
        this.instruccion = "Promotor: Bienvenido al Digitalizador Móvil, favor de Seleccionar una pestaña para continuar";
        this.bProcesoTerminado = false;
        this.icono = -1;
    }

    private Documento(Parcel in) {
        readFromParcel(in);
    }

    public static final Creator<Documento> CREATOR = new Creator<Documento>() {
        @Override
        public Documento createFromParcel(Parcel in) {
            return new Documento(in);
        }

        @Override
        public Documento[] newArray(int size) {
            return new Documento[size];
        }
    };

    public int getId() {
        return identificador;
    }

    public String getNomenclatura() {
        return nomenclatura;
    }

    public int getIcono() {
        return icono;
    }

    public void setIcono(int icono) {
        this.icono = icono;
    }

    public boolean isbProcesoTerminado() {
        return bProcesoTerminado;
    }

    public void setbProcesoTerminado(boolean bProcesoTerminado) {
        this.bProcesoTerminado = bProcesoTerminado;
    }

    public void setPosicion(int posicion){
        this.posicion = posicion;
    }

    public int getPosicion(){
        return posicion;
    }

    @Override
    public String toString() {
        return tipoDocto;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(identificador);
        parcel.writeString(tipoDocto);
        parcel.writeString(nomenclatura);
        parcel.writeString(instruccion);
        parcel.writeByte((byte) (bProcesoTerminado ? 1 : 0));
    }

    private void readFromParcel(Parcel in) {
        identificador = in.readInt();
        tipoDocto = in.readString();
        nomenclatura = in.readString();
        instruccion = in.readString();
        bProcesoTerminado = in.readByte() != 0;
    }
}
