package com.aforecoppelafil.my.digitalizadorafilmovil;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.Fragment;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.aforecoppelafil.my.digitalizadorafilmovil.documento.Documento;
import com.aforecoppelafil.my.digitalizadorafilmovil.webservice.AsyncTaskCompleteListener;
import com.aforecoppelafil.my.digitalizadorafilmovil.webservice.CallWebService;
import com.aforecoppelafil.my.digitalizadorafilmovil.webservice.SoapString;
import com.aforecoppelafil.my.digitalizadorafilmovil.R;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Objects;

import ru.dimorinny.floatingtextbutton.FloatingTextButton;

import static com.aforecoppelafil.my.digitalizadorafilmovil.MainListActivity.mDoctosNoSubidos;
import static com.aforecoppelafil.my.digitalizadorafilmovil.MainListActivity.mNombreDoctos;

/**
 * A simple {@link Fragment} subclass.
 */
public class MainFragment extends Fragment implements AsyncTaskCompleteListener<String>,
        Utilerias.NoticeDialogFragment.NoticeDialogListener,
        Utilerias.InfoDialogFragment.NoticeDialogListener {

    private static final String PROCESO_SERVICIO    = "1";
    private static final String PROCESO_AFILIACION  = "0";
    /**
     * variables de entrada
     */
    private int miIntentosPublicar;
    private static int miProcesosExitosos;
    private String mCurp;
    private String msNomen;
    private String sNombres;
    private String mOpcion;
    private String mRutaImgs;
    private String mFolioAfiSer;
    private String mNumEmpleado;
    private Bundle mDoctosTerminados;
    private boolean mIsTwoPane;
    private boolean mbFlagVarAsignadas;
    private boolean mbFlagConcatNomen;
    private boolean mbFlagFaltanImg;
    private String opcionTramite;
    private boolean opcionDoctos;
    private ArrayList<Documento> ArDocumentos;
    private String CompDom;
    private String EstatusLog = "Digitalizadormovil - MainFragment Version:"+ BuildConfig.VERSION_NAME;
    private String MensajeLog = "";
    private Integer diferencia=0;
    /**
     * variables del layout
     */
    public static FloatingTextButton mBtnPublicar;
    private ConstraintLayout mpbCargando;

    private static final String TAG = "MainFragment";

    public MainFragment() {
        // Required empty public constructor
    }

    public static MainFragment newInstance(Bundle args) {
        MainFragment fragment = new MainFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        boolean bEsServicio;
        String sFolioAfi;
        String sFolioSer;
        if (getArguments() != null) {
            mCurp        = getArguments().getString(Utilerias.TAG_RESP_CURP);
            sFolioAfi    = getArguments().getString(Utilerias.PARAM_FOLIO_AFI);
            sFolioSer    = getArguments().getString(Utilerias.PARAM_FOLIO_SERVICIO);
            mIsTwoPane   = getArguments().getBoolean(Utilerias.PARAM_DOS_PANELES);
            bEsServicio  = getArguments().getBoolean(Utilerias.WS_PARAM_ES_SERVICIO);
            mNumEmpleado = getArguments().getString(Utilerias.PARAM_NUM_EMPLEADO);
            mDoctosTerminados = getArguments().getBundle(Utilerias.PARAM_DOCTOS_FINALIZADOS);
            opcionDoctos = getArguments().getBoolean("OPCION_DOCTOS");
            ArDocumentos = getArguments().getParcelableArrayList("ArDoctos");
            opcionTramite = getArguments().getString(Utilerias.PARAM_OPCION_TRAMITE);
            CompDom        = getArguments().getString("comprobante_domicilio");

            mbFlagConcatNomen  = false;
            mbFlagFaltanImg    = false;
            miIntentosPublicar = 0;
            msNomen  = "";
            sNombres = "";
            mbFlagVarAsignadas = false;
            mRutaImgs = Utilerias.DIR_EXTERNA + Utilerias.DIR_REENROL + sFolioAfi + "/" + Utilerias.DIR_DIGITAL;

            mOpcion = Utilerias.OPCION_AFILIACION_TRASPASO;
            mFolioAfiSer = sFolioAfi;
            if(bEsServicio){
                mOpcion = Utilerias.OPCION_SERVICIOS;
                mFolioAfiSer = sFolioSer;
                Utilerias.writeLog(TAG, "es servicio");
            }
            Utilerias.writeLog(TAG, "Opcion = [" + mOpcion + "] y folio = [" + mFolioAfiSer + "]");
            Utilerias.writeLog(TAG, "opcionTramite = [" + opcionTramite + "]");

        }
        else{
            Utilerias.writeLog("Error","getArgumentes está vacío");
            Utilerias.showInfoDialog(Objects.requireNonNull(getFragmentManager()),
                    this, Utilerias.DIALOG_TAG_ALERT_EMPTY_PARAM);
        }

    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        Utilerias.writeLog("*************MainFragment: onCreateView************* ", "");
        View viu = inflater.inflate(R.layout.fragment_main, container, false);
        TextView tvInstrucciones = viu.findViewById(R.id.tvInstrucciones);
        mBtnPublicar = viu.findViewById(R.id.fab_publicar);
        mpbCargando = viu.findViewById(R.id.ly_cargando);

        mBtnPublicar.setEnabled(false);
        //mBtnPublicar.setClickable(false);
        mBtnPublicar.setOnClickListener(view -> onClickGral(R.id.fab_publicar));
        viu.findViewById(R.id.fab_cancel).setOnClickListener(view -> onClickGral(R.id.fab_cancel));
        Utilerias.writeLog("*************MainFragment: onCreateView fin************* ", "");

        cargarInstrucciones(tvInstrucciones,ArDocumentos);


        // Inflate the layout for this fragmentfragment
        return viu;
    }

    public void cargarInstrucciones(TextView tvInstrucciones, ArrayList<Documento> doctos) {

        if (Integer.parseInt(opcionTramite) == 8) {
            final int strings[] = {
                    R.string.captura_FARE,
                    R.string.captura_IDRE,
                    R.string.captura_FTRE
            };
            String[] nomen = new String[doctos.size()];

            if (!opcionDoctos) {
                nomen = new String[]{
                        Utilerias.DOCTO_FORMATO_REEN,
                        Utilerias.DOCTO_FOTO_REEN,
                        Utilerias.DOCTO_ID_REEN
                };
            } else {
                for (int i = 0; i < doctos.size(); i++) {
                    nomen[i] = doctos.get(i).getNomenclatura();
                    Utilerias.writeLog("TipoDocto:", doctos.get(i).toString());
                }
            }


            int i = 0;
            if (mDoctosTerminados.size() > 0)
                Utilerias.writeLog("LOG", "1");
            for (; i < nomen.length; i++)
                if (!mDoctosTerminados.getBoolean(nomen[i])) {
                    Utilerias.writeLog("LOG", "" + i);
                    if (!opcionDoctos) {
                        tvInstrucciones.setText(strings[i]);
                    } else {
                        tvInstrucciones.setText("Promotor: Favor de Continuar con la Captura de " + doctos.get(i).toString());
                    }
                    i = 10;
                }
            if (i == nomen.length) {
                tvInstrucciones.setText(R.string.captura_Publicar);
                mBtnPublicar.setEnabled(true);
                mBtnPublicar.setBackgroundColor(getResources().getColor(R.color.colorOk));
            }

        }else{
            final int strings[] = {
                    R.string.item_instrucciones,
                    R.string.captura_FARE,
                    R.string.captura_IDRE,
                    R.string.captura_FTRE
            };
            String[] nomen = new String[doctos.size()];

            Utilerias.writeLog(TAG, "doctos.size()  " + doctos.size() );

            if(doctos.size() != 0) {
                for (int i = 0; i < doctos.size(); i++) {
                    nomen[i] = doctos.get(i).getNomenclatura();
                    Utilerias.writeLog("TipoDocto:", doctos.get(i).toString());
                }
            }else{
                nomen = new String[]{
                        Utilerias.DOCTO_FORMATO_REEN,
                        Utilerias.DOCTO_FOTO_REEN,
                        Utilerias.DOCTO_ID_REEN
                };
            }
            //}

            Utilerias.writeLog("LOG","1");
            int i = 0;
            if(mDoctosTerminados.size() > 0)
                Utilerias.writeLog("LOG","1");
            for (; i < nomen.length; i++)
                if(!mDoctosTerminados.getBoolean(nomen[i])) {
                    Utilerias.writeLog("LOG",""+i);
                    if(!opcionDoctos){
                        tvInstrucciones.setText(strings[i]);
                    }else{
                        tvInstrucciones.setText("Promotor: Favor de Continuar con la Captura de "+doctos.get(i).toString());
                    }
                    i=10;
                }

            if(i==nomen.length) {

                tvInstrucciones.setText(R.string.captura_Publicar);
                mBtnPublicar.setEnabled(true);
                mBtnPublicar.setBackgroundColor(getResources().getColor(R.color.colorOk));
            }

            diferencia = DoctoListFragment.iContDoc - DoctoListFragment.iDocTerm ;

            if(Integer.parseInt(opcionTramite) == 1 && (diferencia > 1 || diferencia == 0)){

                DoctoListFragment.incrementarCapturados();
            }

            if(Integer.parseInt(opcionTramite) == 1 && diferencia == 1){

                tvInstrucciones.setText(R.string.captura_Publicar);
                mBtnPublicar.setEnabled(true);
                mBtnPublicar.setBackgroundColor(getResources().getColor(R.color.colorOk));

            }

            if(Integer.parseInt(CompDom) == 1 && Integer.parseInt(opcionTramite) == 3 && diferencia == 1){

                tvInstrucciones.setText(R.string.captura_Publicar);
                mBtnPublicar.setEnabled(true);
                mBtnPublicar.setBackgroundColor(getResources().getColor(R.color.colorOk));
            }



        }
    }


    public void onClickGral(int id) {
        switch (id){
            case R.id.fab_publicar:{
                if(mBtnPublicar.isEnabled()){
                    if(miIntentosPublicar < 3)
                        /*Utilerias.showInfoDialog(Objects.requireNonNull(getFragmentManager()),
                            this, Utilerias.DIALOG_TAG_ALERT_PUBLICAR);*/
                        publicarExpediente();
                    else
                        Utilerias.showInfoDialog(Objects.requireNonNull(getFragmentManager()),
                                this, Utilerias.DIALOG_TAG_ALERT_NUM_INT_SUP);
                }

                break;
            }
            case R.id.fab_cancel:{
                Utilerias.showNoticeDialog(Objects.requireNonNull(getFragmentManager()),
                        this, Utilerias.DIALOG_TAG_CANCELAR_DIGI);
                break;
            }
            default:
                Utilerias.writeLog("Error", "entró en el campo default del onClick " + id);
                Utilerias.writeLog("Error", "picture " + R.id.fab_picture + " done " +
                        R.id.fab_done + " cancel " + R.id.fab_cancel);
        }
    }//onClick

    private void subirImgsFaltantes(ArrayList<String> doctos, String sRuta){
        HashMap<String, byte[]> imgDatas = getListFiles(doctos, new File(sRuta));
        if(imgDatas.size() > 0){
            Iterator it = imgDatas.entrySet().iterator();
            while (it.hasNext()) {
                Map.Entry pair = (Map.Entry)it.next();
                soapSubirImgServidor(pair.getKey().toString(), (byte[])pair.getValue());
                it.remove(); // avoids a ConcurrentModificationException
            }
        }
        else
            Utilerias.writeLog(TAG, "el directorio no fue creado");

    }

    private HashMap<String, byte[]> getListFiles(ArrayList<String> doctos, File parentDir) {
            HashMap<String, byte[]> inBytes = new HashMap<>();
        if(parentDir.exists()) {
            File[] files = parentDir.listFiles();
            int i = 0;
            for (File file : files) {
                if (file.getName().endsWith(doctos.get(i) + ".tif")) {
                    int size = (int) file.length();
                    byte[] bytes = new byte[size];
                    try {
                        BufferedInputStream buf = new BufferedInputStream(new FileInputStream(file));
                        buf.read(bytes, 0, bytes.length);
                        buf.close();
                        inBytes.put(file.getName(), bytes);
                    } catch (IOException e) {
                        e.printStackTrace();
                        return inBytes;
                    }
                }
                i++;
            }
        }
        return inBytes;
    }

    private void soapSubirImgServidor(String nombreImg, byte[] byteImg){
        //Hago el tratado de la imagen a base64
        Utilerias.writeLog(TAG+" WS ","[soapSubirImgServidor] nombreImg " + nombreImg);

        String encodedImage = Base64.encodeToString(byteImg, Base64.NO_WRAP);

        SoapString sSoap = new SoapString(Utilerias.WS_MODULO_MOVIL, Utilerias.WS_METODO_SUBIR_IMG_SERVIDOR);
        sSoap.setParams(Utilerias.WS_PARAM_NOMBRE_IMAGEN, nombreImg);
        sSoap.setParams(Utilerias.WS_PARAM_IMAGEN_BASE64, encodedImage);

        CallWebService callWS = new CallWebService(this, Utilerias.SOAP_SUBIR_IMG_SERVIDOR);
        callWS.setFlag(true);
        callWS.execute(sSoap.getSoapString());
    }

    private void soapMoverImgsIntermedio(String sFolioAfiSer, String iOpcion) {

        Utilerias.writeLog(TAG+" WS ","[soapMoverImgsIntermedio] nombreImg " + sFolioAfiSer);
        Utilerias.writeLog(TAG+" WS ","[soapMoverImgsIntermedio] iOpcion " + iOpcion);

        SoapString sSoap = new SoapString(Utilerias.WS_MODULO_MOVIL, Utilerias.WS_METODO_MOVER_IMGS_INTERMEDIO);
        sSoap.setParams(Utilerias.WS_PARAM_FOLIO, sFolioAfiSer);
        sSoap.setParams(Utilerias.WS_PARAM_OPCION, iOpcion);

        CallWebService callWS =
                new CallWebService(this, Utilerias.SOAP_MOVER_IMGS_INTERMEDIO);
        callWS.setFlag(true);
        callWS.execute(sSoap.getSoapString());
    }

    private void soapConcatenarNuevasNomen(String iOpcion, String iFolioAfiSer,
                                           String iNumEmp, String sDoctosIndexar) {
        Utilerias.writeLog(TAG+" WS ","[soapConcatenarNuevasNomen] iOpcion " + iOpcion);
        Utilerias.writeLog(TAG+" WS ","[soapConcatenarNuevasNomen] iFolioAfiSer " + iFolioAfiSer);
        Utilerias.writeLog(TAG+" WS ","[soapConcatenarNuevasNomen] iNumEmp " + iNumEmp);

        Utilerias.writeLog(TAG+" WS ","[soapConcatenarNuevasNomen] opcionTramite " + opcionTramite);

        if(Integer.parseInt(opcionTramite)  == 3){
            sDoctosIndexar = sDoctosIndexar + "|CURP|ID00|FAHD";
        }

        Utilerias.writeLog(TAG+" WS ","[soapConcatenarNuevasNomen] sDoctosIndexar " + sDoctosIndexar);

        SoapString sSoap = new SoapString(Utilerias.WS_MODULO_MOVIL, Utilerias.WS_METODO_CONCAT_NUEVAS_NOMEN);
        sSoap.setParams(Utilerias.WS_PARAM_OPCION, iOpcion);
        sSoap.setParams(Utilerias.WS_PARAM_FOLIO, iFolioAfiSer);
        sSoap.setParams(Utilerias.WS_PARAM_NUM_EMPLEADO, iNumEmp);
        sSoap.setParams(Utilerias.WS_PARAM_DOCTOS_INDEXAR, sDoctosIndexar);
        sSoap.setParams("iTabla",opcionTramite);
        CallWebService callWS =
                new CallWebService(this, Utilerias.SOAP_CONCAT_NUEVAS_NOMENCLATURAS);
        callWS.setFlag(true);
        callWS.execute(sSoap.getSoapString());
    }

    private void soapInsertarImgsDigi(String iOpcion, String esServicio, String iFolioAfiSer,
                                      String sMacAddress, String arrNombreArchivos) {
        Utilerias.writeLog(TAG+" WS ","[soapConcatenarNuevasNomen] iOpcion " + iOpcion);
        Utilerias.writeLog(TAG+" WS ","[soapConcatenarNuevasNomen] esServicio " + esServicio);
        Utilerias.writeLog(TAG+" WS ","[soapConcatenarNuevasNomen] iFolioAfiSer " + iFolioAfiSer);
        Utilerias.writeLog(TAG+" WS ","[soapConcatenarNuevasNomen] sMacAddress " + sMacAddress);
        Utilerias.writeLog(TAG+" WS ","[soapConcatenarNuevasNomen] arrNombreArchivos " + arrNombreArchivos);
        SoapString sSoap = new SoapString(Utilerias.WS_MODULO_MOVIL, Utilerias.WS_METODO_INSERTAR_DOCTOS_DIGI);
        sSoap.setParams(Utilerias.WS_PARAM_OPCION, iOpcion);
        sSoap.setParams(Utilerias.WS_PARAM_ES_SERVICIO, esServicio);
        sSoap.setParams(Utilerias.WS_PARAM_FOLIO, iFolioAfiSer);
        sSoap.setParams(Utilerias.WS_PARAM_MAC_ADDRES, sMacAddress);
        sSoap.setParams(Utilerias.WS_PARAM_NOMBRES_ARCH, arrNombreArchivos);

        CallWebService callWS =
                new CallWebService(this, Utilerias.SOAP_INSERTAR_DOCTOS_DIGI);
        callWS.setFlag(true);
        callWS.execute(sSoap.getSoapString());
    }

    private void soapActualizarEstatusCol(String sFolioAfiSer, String iOpcion) {
        Utilerias.writeLog(TAG+" WS ","[soapActualizarEstatusCol] iOpcion " + iOpcion);
        Utilerias.writeLog(TAG+" WS ","[soapActualizarEstatusCol] sFolioAfiSer " + sFolioAfiSer);
        SoapString sSoap = new SoapString(Utilerias.WS_MODULO_MOVIL, Utilerias.WS_METODO_ACTUALIZAR_ESTATUS_COL);
        sSoap.setParams(Utilerias.WS_PARAM_FOLIO, sFolioAfiSer);
        sSoap.setParams(Utilerias.WS_PARAM_OPCION, iOpcion);

        CallWebService callWS =
                new CallWebService(this, Utilerias.SOAP_ACTUALIZAR_ESTATUS_COL);
        callWS.setFlag(true);
        callWS.execute(sSoap.getSoapString());
    }

    private void soapActualizarEstatusReenrol(String sCurp, String iEstatusProceso) {
        Utilerias.writeLog(TAG+" WS ","[soapActualizarEstatusCol] sCurp " + sCurp);
        Utilerias.writeLog(TAG+" WS ","[soapActualizarEstatusCol] iEstatusProceso " + iEstatusProceso);
        SoapString sSoap = new SoapString(Utilerias.WS_MODULO_MOVIL, Utilerias.WS_METODO_ACTUALIZAR_ESTATUS_REEN);
        sSoap.setParams(Utilerias.WS_PARAM_ESTATUS_DIGI, iEstatusProceso);
        sSoap.setParams(Utilerias.WS_PARAM_CURP, sCurp);
        sSoap.setParams("iOpcionDigitalizador", opcionTramite);
        sSoap.setParams("cFolio", mFolioAfiSer);

        CallWebService callWS =
                new CallWebService(this, Utilerias.SOAP_ACTUALIZAR_ESTATUS_REEN);
        callWS.setFlag(true);
        callWS.execute(sSoap.getSoapString());
    }

    private void publicarExpediente(){
        mpbCargando.setVisibility(View.VISIBLE);
        //showToast(getActivity(),"Publicando...");
        //mDoctosSubidos es un ArrayList<String> de MainListActivity
        // que contiene los documentos que se lograron subir
        //mNombreDoctos es un ArrayList<String> de MainListActivity
        // que contiene el nombre de los documentos digitalizados
        //Se le quita 1 porque hay un documento autoindexado
        if(Integer.parseInt(opcionTramite) == 1) {
            if(DoctoListFragment.iDocCap > 1){
                Utilerias.writeLog(TAG, " ***** DoctoListFragment.iDocCap **** " +DoctoListFragment.iDocCap);
                if (mDoctosNoSubidos.size() != 0) {
                    mbFlagFaltanImg = true;
                    Utilerias.writeLog(TAG, "no se subieron todos los documentos");
                    subirImgsFaltantes(mDoctosNoSubidos, mRutaImgs);
                } else
                    Utilerias.writeLog(TAG, "todos los documentos fueron subidos exitosamente");
                soapMoverImgsIntermedio(mFolioAfiSer, mOpcion);
            }else {

                Objects.requireNonNull(getActivity()).
                        setResult(Activity.RESULT_OK, new Intent());
                getActivity().finishAndRemoveTask();
            }
        }else{
            if (mDoctosNoSubidos.size() != 0) {
                mbFlagFaltanImg = true;
                Utilerias.writeLog(TAG, "no se subieron todos los documentos");
                subirImgsFaltantes(mDoctosNoSubidos, mRutaImgs);
            } else
                Utilerias.writeLog(TAG, "todos los documentos fueron subidos exitosamente");
            soapMoverImgsIntermedio(mFolioAfiSer, mOpcion);
        }
    }

    private void soapGuardarLogs(){
        SoapString sSoap = new SoapString("wsafiliacionmovil",
                "GuardarLogs");

        sSoap.setParams("Mensaje", MensajeLog);
        sSoap.setParams("Estatus", EstatusLog);

        CallWebService callWS = new CallWebService(null, 0);
        callWS.setFlag(false);
        callWS.execute(sSoap.getSoapString());
    }

    @Override
    public void onTaskComplete(String xmlMen, int id) {
        if(xmlMen.equals("")){
            Utilerias.writeLog(TAG+"ERROR","Error al procesar la respuesta del servicio xmlMen está vacío");
            Utilerias.showInfoDialog(Objects.requireNonNull(getFragmentManager()),
                    this, Utilerias.DIALOG_TAG_ALERT_WS_FALLO);
            return;
        }
        Bundle values = Utilerias.getBundleValuesXML(xmlMen);
        if(values == null) {
            Utilerias.writeLog(TAG+"ERROR", "VALUES ES NULO EN onTaskComplete");
            Utilerias.showInfoDialog(Objects.requireNonNull(getFragmentManager()),
                    this, Utilerias.DIALOG_TAG_ALERT_WS_FALLO);
        }
        //Asigno los estatus a Variables para a continuación removerlos del Bundle
        int iEstatus = Integer.parseInt(Objects.requireNonNull(values).getString(Utilerias.WS_TAG_HTTP_ESTADO));
        String sMensaje = values.getString(Utilerias.WS_TAG_MSG);
        //Remuevo los valores de retorno del Bundle
        values.remove(Utilerias.WS_TAG_HTTP_ESTADO);
        values.remove(Utilerias.WS_TAG_MSG);

        switch(iEstatus){
            case Utilerias.RESP_HTTP_OK:
            case Utilerias.RESP_HTTP_OK_MOD:{
                switch (id){
                    case Utilerias.SOAP_SUBIR_IMG_SERVIDOR: {
                        Utilerias.writeLog(TAG, "Se subieron todas las imagenes correctamente");
                        miProcesosExitosos ++;
                        break;
                    }
                    case Utilerias.SOAP_MOVER_IMGS_INTERMEDIO: {
                        Utilerias.writeLog(TAG, "Se movieron imagenes correctamente");
                        miProcesosExitosos ++;
                        //mNombreDoctos es una variable estática que se encuentra en el mainListActivity
                        Iterator it = mNombreDoctos.entrySet().iterator();
                        StringBuilder sbNomen = new StringBuilder();
                        StringBuilder sbNombres = new StringBuilder();
                        while (it.hasNext()) {
                            Map.Entry pair = (Map.Entry)it.next();
                            sbNomen.append("|").append(pair.getKey().toString());
                            sbNombres.append("<Archivo>").append("<cNombreArchivo>").
                                    append(pair.getValue().toString()).
                                append("</cNombreArchivo>").append("</Archivo>");
                            it.remove(); // avoids a ConcurrentModificationException
                        }
                        if(!mbFlagVarAsignadas){
                            msNomen  = sbNomen.toString();
                            sNombres = sbNombres.toString();
                            mbFlagVarAsignadas = true;
                        }
                        Utilerias.writeLog(TAG, "Nomenclaturas " + msNomen);
                        //Se valida que si ya se hizo esta operación exitosamente, no se vuelva a realizar
                        if(!mbFlagConcatNomen){
                            //Se ejecuta el soap que actualiza en la colimagenes
                            soapConcatenarNuevasNomen(mOpcion, mFolioAfiSer,
                                    mNumEmpleado, msNomen
                            );
                        }
                        //Si la opcion es servicios, entonces se le manda un 1 si no, un 0
                        String esServicio = PROCESO_AFILIACION;
                        if(mOpcion.equals(Utilerias.OPCION_SERVICIOS))
                            esServicio = PROCESO_SERVICIO;

                        Utilerias.writeLog(TAG, "Nombres Imagenes\n" + sNombres);
                        //se ejecuta el soap que inserta en la colimagenesindex
                        soapInsertarImgsDigi(
                                mOpcion, esServicio, mFolioAfiSer,
                                Utilerias.getMacAddr(), sNombres
                        );
                        break;
                    }
                    case Utilerias.SOAP_CONCAT_NUEVAS_NOMENCLATURAS: {
                        int resp = Integer.parseInt(values.getString(Utilerias.TAG_RESP_INT));
                        if(resp == 1){
                            mbFlagConcatNomen = true;
                            miProcesosExitosos ++;
                            Utilerias.writeLog(TAG, "Se actualizaron las nomenclaturas");

                        }else
                            Utilerias.writeLog(TAG, "No se concatenaron NUEVAS_NOMENCLATURAS");
                        break;
                    }
                    case Utilerias.SOAP_INSERTAR_DOCTOS_DIGI: {
                        int resp = Integer.parseInt(values.getString(Utilerias.TAG_RESP_INT));
                        if(resp == 1){
                            miProcesosExitosos ++;
                            Utilerias.writeLog(TAG, "Se subieron todas las imagenes correctamente");

                            soapActualizarEstatusCol(mFolioAfiSer, mOpcion);
                        }else
                            Utilerias.writeLog(TAG, "No se insertaron DOCTOS_DIGI");
                        break;
                    }
                    case Utilerias.SOAP_ACTUALIZAR_ESTATUS_COL: {
                        int resp = Integer.parseInt(values.getString(Utilerias.TAG_RESP_INT));
                        if(resp == 1){
                            miProcesosExitosos ++;
                            if(mbFlagConcatNomen)
                                soapActualizarEstatusReenrol(mCurp, Utilerias.ESTATUS_PROCESO_PUBLICADO);
                            else{
                                mpbCargando.setVisibility(View.GONE);
                                Utilerias.writeLog(TAG, "No se ejecutaron todas las operaciones, " +
                                        "se volverá a intentar");
                                Utilerias.showInfoDialog(Objects.requireNonNull(getFragmentManager()),
                                        this, Utilerias.DIALOG_TAG_ALERT_FALLO_PUB);
                            }
                            Utilerias.writeLog(TAG, "Se actualizó ESTATUS_COL");
                        }else{
                            mpbCargando.setVisibility(View.GONE);
                            Utilerias.writeLog(TAG, "No se actualizó ESTATUS_COL");
                            Utilerias.showInfoDialog(Objects.requireNonNull(getFragmentManager()),
                                    this, Utilerias.DIALOG_TAG_ALERT_FALLO_PUB);
                        }

                        break;
                    }
                    case Utilerias.SOAP_ACTUALIZAR_ESTATUS_REEN: {
                        miProcesosExitosos ++;
                        Utilerias.writeLog(TAG, "Se actualizó ESTATUS_REEN");
                        mpbCargando.setVisibility(View.GONE);

                        if((mbFlagFaltanImg && miProcesosExitosos == 6) ||
                                (!mbFlagFaltanImg && miProcesosExitosos == 5) ||
                                (!mbFlagFaltanImg && mbFlagConcatNomen && miProcesosExitosos == 4)){
                            Objects.requireNonNull(getActivity()).
                                    setResult(Activity.RESULT_OK, new Intent());
                            getActivity().finishAndRemoveTask();
                        }else{
                            Utilerias.writeLog(TAG, "Falló al actualizar el estatus del reenrolamiento EN LA BD"
                            + " El numero de procesos éxitosos son = " + miProcesosExitosos);
                            Utilerias.showInfoDialog(Objects.requireNonNull(getFragmentManager()),
                                    this, Utilerias.DIALOG_TAG_ALERT_FALLO_PUB);
                        }

                        break;
                    }
                    default:
                        Utilerias.writeLog(TAG,"HttpResponse: " + iEstatus + " idOp: " + id + ": " +
                                "Esto no debe de pasar en onTaskComplete switch id okWS");
                }
                break;
            }
            case Utilerias.RESP_HTTP_ERR_ACCESO:
            case Utilerias.RESP_HTTP_ERR_NO_ENCONTRADO:
            case Utilerias.RESP_HTTP_ERR_SERVIDOR:
            case Utilerias.RESP_HTTP_ERR_PARAM:{
                MensajeLog = "HttpResponse:" + iEstatus +" idOperación:" + id + ":" + sMensaje;
                soapGuardarLogs();
                MensajeLog = "CURP: "+mCurp+"| NUMERO_EMPLEADO: "+mNumEmpleado;
                soapGuardarLogs();
                Utilerias.writeLog(TAG,"HttpResponse: " + iEstatus + " idOperación: " + id + ": " + sMensaje);
                switch (id){
                    case Utilerias.SOAP_SUBIR_IMG_SERVIDOR: {
                        Utilerias.writeLog(TAG, "Falló al tratar de subir las imagenes al servidor");
                        break;
                    }
                    case Utilerias.SOAP_MOVER_IMGS_INTERMEDIO: {
                        Utilerias.writeLog(TAG, "Falló al mover las imagenes al intermedio");
                        break;
                    }
                    case Utilerias.SOAP_CONCAT_NUEVAS_NOMENCLATURAS: {
                        Utilerias.writeLog(TAG, "Falló al concatenar las nuevas nomenclaturas");
                        break;
                    }
                    case Utilerias.SOAP_INSERTAR_DOCTOS_DIGI: {
                        Utilerias.writeLog(TAG, "Falló al insertar los documentos digitalizados");
                        break;
                    }
                    case Utilerias.SOAP_ACTUALIZAR_ESTATUS_COL: {
                        mpbCargando.setVisibility(View.GONE);
                        Utilerias.showInfoDialog(Objects.requireNonNull(getFragmentManager()),
                                this, Utilerias.DIALOG_TAG_ALERT_FALLO_PUB);
                        Utilerias.writeLog(TAG, "Falló al actualizar el estatus de los documentos digitalizados");
                        break;
                    }
                    case Utilerias.SOAP_ACTUALIZAR_ESTATUS_REEN: {
                        mpbCargando.setVisibility(View.GONE);
                        Utilerias.showInfoDialog(Objects.requireNonNull(getFragmentManager()),
                                this, Utilerias.DIALOG_TAG_ALERT_FALLO_PUB);
                        Utilerias.writeLog(TAG, "Falló al actualizar el estatus del reenrolamiento EN EL SVR");
                        break;
                    }
                    default:
                        Utilerias.writeLog(TAG,"HttpResponse: " + iEstatus + " idOp: " + id + ": " +
                                "Esto no debe de pasar en onTaskComplete switch id errorWS");
                }
                break;
            }
            default:
                Utilerias.writeLog(TAG,"HttpResponse: " + iEstatus + " idOp: " + id + ": " +
                        "Esto no debe de pasar en onTaskComplete switch wsResponse");
        }

    }

    @Override
    public void onDialogPositiveClick(int iTag) {
        switch (iTag){
            case Utilerias.DIALOG_TAG_CANCELAR_DIGI:{
                Utilerias.terminarAplicacion(Objects.requireNonNull(getActivity()), Utilerias.ERROR_RESPONSE_BTN_CANCEL);
                break;
            }
            case Utilerias.DIALOG_TAG_ALERT_PUBLICAR:{
                publicarExpediente();
                break;
            }
            case Utilerias.DIALOG_TAG_ALERT_WS_FALLO:
            case Utilerias.DIALOG_TAG_ALERT_NUM_INT_SUP:{
                Utilerias.terminarAplicacion(Objects.requireNonNull(getActivity()), Utilerias.ERROR_RESPONSE_WS_FAILED);
                break;
            }
            case Utilerias.DIALOG_TAG_ALERT_EMPTY_PARAM:{
                Utilerias.terminarAplicacion(Objects.requireNonNull(getActivity()), Utilerias.ERROR_RESPONSE_EMPTY_PARAMS);
                break;
            }
            case Utilerias.DIALOG_TAG_ALERT_FALLO_PUB:{
                miProcesosExitosos = 0;
                miIntentosPublicar++;
                break;
            }
        }
    }

    @Override
    public void onDialogNegativeClick(int iTag) {
        switch (iTag){
            case Utilerias.DIALOG_TAG_CANCELAR_DIGI:{
                Utilerias.writeLog(TAG, "cancelo la cancelacion");
                break;
            }
            default:
                Utilerias.writeLog(TAG, "cancelo algo no identificado");
        }
    }

}

