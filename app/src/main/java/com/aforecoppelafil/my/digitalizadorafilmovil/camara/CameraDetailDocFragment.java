package com.aforecoppelafil.my.digitalizadorafilmovil.camara;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.widget.AppCompatImageView;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.aforecoppelafil.my.digitalizadorafilmovil.documento.Documento;
import com.aforecoppelafil.my.digitalizadorafilmovil.webservice.AsyncTaskCompleteListener;
import com.aforecoppelafil.my.digitalizadorafilmovil.webservice.CallWebService;
import com.aforecoppelafil.my.digitalizadorafilmovil.webservice.SoapString;
import com.aforecoppelafil.my.digitalizadorafilmovil.DoctoListFragment;
import com.aforecoppelafil.my.digitalizadorafilmovil.R;
import com.aforecoppelafil.my.digitalizadorafilmovil.Utilerias;
import com.scanlibrary.ScanActivity;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Objects;

import ru.dimorinny.floatingtextbutton.FloatingTextButton;

import static com.aforecoppelafil.my.digitalizadorafilmovil.DoctoListFragment.mEsPrimeraVez;
import static com.aforecoppelafil.my.digitalizadorafilmovil.DoctoListFragment.mLvDoctos;
import static com.aforecoppelafil.my.digitalizadorafilmovil.MainListActivity.mDoctosNoSubidos;
import static com.aforecoppelafil.my.digitalizadorafilmovil.Utilerias.DIALOG_TAG_ALERT_CAPTURA_FOTO;
import static com.aforecoppelafil.my.digitalizadorafilmovil.Utilerias.DIALOG_TAG_ALERT_EMPTY_PARAM;
import static com.aforecoppelafil.my.digitalizadorafilmovil.Utilerias.DIALOG_TAG_ALERT_WS_FALLO;
import static com.aforecoppelafil.my.digitalizadorafilmovil.Utilerias.DIALOG_TAG_CANCELAR_DIGI;
import static com.aforecoppelafil.my.digitalizadorafilmovil.Utilerias.DIALOG_TAG_CONFIRM;
import static com.aforecoppelafil.my.digitalizadorafilmovil.Utilerias.DIALOG_TAG_CONFIRM_RECAP;
import static com.aforecoppelafil.my.digitalizadorafilmovil.Utilerias.DIR_EXTERNA;
import static com.aforecoppelafil.my.digitalizadorafilmovil.Utilerias.DIR_REENROL;
import static com.aforecoppelafil.my.digitalizadorafilmovil.Utilerias.DIR_TMP;
import static com.aforecoppelafil.my.digitalizadorafilmovil.Utilerias.DOCTO_FOTO_REEN;
import static com.aforecoppelafil.my.digitalizadorafilmovil.Utilerias.ERROR_RESPONSE_BTN_CANCEL;
import static com.aforecoppelafil.my.digitalizadorafilmovil.Utilerias.ERROR_RESPONSE_DIR_NO_EXISTE;
import static com.aforecoppelafil.my.digitalizadorafilmovil.Utilerias.ERROR_RESPONSE_EMPTY_PARAMS;
import static com.aforecoppelafil.my.digitalizadorafilmovil.Utilerias.ERROR_RESPONSE_WS_FAILED;
import static com.aforecoppelafil.my.digitalizadorafilmovil.Utilerias.PARAM_DOCTO;
import static com.aforecoppelafil.my.digitalizadorafilmovil.Utilerias.PARAM_DOS_PANELES;
import static com.aforecoppelafil.my.digitalizadorafilmovil.Utilerias.PARAM_FOLIO_AFI;
import static com.aforecoppelafil.my.digitalizadorafilmovil.Utilerias.RESP_HTTP_ERR_ACCESO;
import static com.aforecoppelafil.my.digitalizadorafilmovil.Utilerias.RESP_HTTP_ERR_NO_ENCONTRADO;
import static com.aforecoppelafil.my.digitalizadorafilmovil.Utilerias.RESP_HTTP_ERR_PARAM;
import static com.aforecoppelafil.my.digitalizadorafilmovil.Utilerias.RESP_HTTP_ERR_SERVIDOR;
import static com.aforecoppelafil.my.digitalizadorafilmovil.Utilerias.RESP_HTTP_OK;
import static com.aforecoppelafil.my.digitalizadorafilmovil.Utilerias.RESP_HTTP_OK_MOD;
import static com.aforecoppelafil.my.digitalizadorafilmovil.Utilerias.SOAP_SUBIR_IMG_SERVIDOR_FTRE;
import static com.aforecoppelafil.my.digitalizadorafilmovil.Utilerias.TAG_RESP_CURP;
import static com.aforecoppelafil.my.digitalizadorafilmovil.Utilerias.TAG_RESP_FECHA;
import static com.aforecoppelafil.my.digitalizadorafilmovil.Utilerias.TAG_RESP_FOLIO;
import static com.aforecoppelafil.my.digitalizadorafilmovil.Utilerias.TAG_RESP_TIPO_SOL;
import static com.aforecoppelafil.my.digitalizadorafilmovil.Utilerias.WS_METODO_SUBIR_IMG_SERVIDOR;
import static com.aforecoppelafil.my.digitalizadorafilmovil.Utilerias.WS_MODULO_MOVIL;
import static com.aforecoppelafil.my.digitalizadorafilmovil.Utilerias.WS_PARAM_IMAGEN_BASE64;
import static com.aforecoppelafil.my.digitalizadorafilmovil.Utilerias.WS_PARAM_NOMBRE_IMAGEN;
import static com.aforecoppelafil.my.digitalizadorafilmovil.Utilerias.WS_TAG_HTTP_ESTADO;
import static com.aforecoppelafil.my.digitalizadorafilmovil.Utilerias.WS_TAG_MSG;
import static com.aforecoppelafil.my.digitalizadorafilmovil.Utilerias.cambiarIcono;
import static com.aforecoppelafil.my.digitalizadorafilmovil.Utilerias.getBundleValuesXML;
import static com.aforecoppelafil.my.digitalizadorafilmovil.Utilerias.guardarArchivo;
import static com.aforecoppelafil.my.digitalizadorafilmovil.Utilerias.showInfoDialog;
import static com.aforecoppelafil.my.digitalizadorafilmovil.Utilerias.showNoticeDialog;
import static com.aforecoppelafil.my.digitalizadorafilmovil.Utilerias.terminarAplicacion;
import static com.aforecoppelafil.my.digitalizadorafilmovil.Utilerias.tratarImagen;
import static com.aforecoppelafil.my.digitalizadorafilmovil.Utilerias.writeLog;

public class CameraDetailDocFragment extends Fragment implements AsyncTaskCompleteListener<String>,
        Utilerias.NoticeDialogFragment.NoticeDialogListener,
        Utilerias.InfoDialogFragment.NoticeDialogListener{
    private static final int CAMINO_FOTO = 77;

    private static final String TAG = "CameraDetailFragment";

    private String mCurp;
    private String mFecha;
    private String mTipoOp;
    private String mFolioAfi;
    private String mFolioAfiSer;
    private String mCurrentPhotoPath;
    private boolean mIsTwoPane;
    private boolean esRecaptura = false;

    private Bitmap mBitmapFinal;
    private TextView mTexto;
    private AppCompatImageView mFoto;
    private FloatingTextButton mFabDone;
    /*private String DoctoNom;
    private int DoctoPos;
    private String Docto;*/
    private Documento docto;

    private OnFragmentInteractionListener mListener;

    public CameraDetailDocFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param args Parameter 1.
     * @return A new instance of fragment DoctoDetailFragment.
     */
    public static CameraDetailDocFragment newInstance(Bundle args) {
        CameraDetailDocFragment fragment = new CameraDetailDocFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mCurp        = getArguments().getString(TAG_RESP_CURP);
            mFecha       = getArguments().getString(TAG_RESP_FECHA);
            mTipoOp      = getArguments().getString(TAG_RESP_TIPO_SOL);
            mFolioAfi    = getArguments().getString(PARAM_FOLIO_AFI);
            mIsTwoPane   = getArguments().getBoolean(PARAM_DOS_PANELES);
            mFolioAfiSer = getArguments().getString(TAG_RESP_FOLIO);
            docto        = getArguments().getParcelable(PARAM_DOCTO);
            /*DoctoPos     = getArguments().getInt("posDocto");
            Docto        = getArguments().getString("doctoIc");
            DoctoNom     = getArguments().getString("DoctoNom");*/
        }
        else{
            writeLog("Error","getArgumentes está vacío");
            showInfoDialog(Objects.requireNonNull(getFragmentManager()),
                    this, DIALOG_TAG_ALERT_EMPTY_PARAM);
        }
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_camera_detail_doc,
                container, false);
        mFabDone = view.findViewById(R.id.fab_done_doc);
        mFabDone.setEnabled(false);

        mFabDone.setOnClickListener(viu -> onClickGral(R.id.fab_done_doc));
        view.findViewById(R.id.fab_cancel_doc).setOnClickListener(viu -> onClickGral(R.id.fab_cancel_doc));
        view.findViewById(R.id.fab_picture_doc).setOnClickListener(viu -> onClickGral(R.id.fab_picture_doc));

        mFoto = view.findViewById(R.id.ivFoto);
        mTexto = view.findViewById(R.id.tvInstrucciones);

        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof CameraDetailDocFragment.OnFragmentInteractionListener) {
            mListener = (CameraDetailDocFragment.OnFragmentInteractionListener) context;


        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public void onClickGral(int id) {
        switch (id){
            case R.id.fab_picture_doc:{
                if(esRecaptura)
                    capturarFotoTrab();
                    /*showNoticeDialog(Objects.requireNonNull(getFragmentManager()),
                            this, DIALOG_TAG_CONFIRM_RECAP); */
                else
                    capturarFotoTrab();
                break;
            }
            case R.id.fab_done_doc:{
                if(mFabDone.isEnabled())
                    if(mBitmapFinal == null){
                        showInfoDialog(Objects.requireNonNull(getFragmentManager()),
                                this, DIALOG_TAG_ALERT_CAPTURA_FOTO);
                    }else{
                        aceptarImagen();
                        /*showNoticeDialog(Objects.requireNonNull(getFragmentManager()),
                                this, DIALOG_TAG_CONFIRM);*/
                    }
                break;
            }
            case R.id.fab_cancel_doc:{
                showNoticeDialog(Objects.requireNonNull(getFragmentManager()),
                        this, DIALOG_TAG_CANCELAR_DIGI);
//                closeTab();
                break;
            }
            default:
                writeLog("Error", "entró en el campo default del onClick " + id);
                writeLog("Error", "picture " + R.id.fab_picture_doc + " done " +
                        R.id.fab_done_doc + " cancel " + R.id.fab_cancel_doc);
        }
    }//onClick

    private void capturarFotoTrab() {
        Intent intent = new Intent(getContext(), ScanActivity.class);
        // Set title color - optional
        intent.putExtra("TipoDocumento", 0);
        intent.putExtra(ScanActivity.EXTRA_ACTION_BAR_COLOR, R.color.colorPrimaryDark);
        // Set language - optional
        intent.putExtra(ScanActivity.EXTRA_LANGUAGE, "es");
        startActivityForResult(intent, CAMINO_FOTO);
        /*Intent intentPic = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

        if (intentPic.resolveActivity(Objects.requireNonNull(getActivity()).getPackageManager()) != null) {
            File photoFile = null;
            try {
                photoFile = createImageFile();
            } catch (IOException ex) {
                ex.printStackTrace();
            }
            // Continue only if the File was successfully created
            if (photoFile != null) {
                writeLog("fab_picture", mCurrentPhotoPath);
                Uri photoURI = FileProvider.getUriForFile(Objects.requireNonNull(getContext()),
                        getString(R.string.file_provider_authority),
                        photoFile);
                writeLog("fab_picture2", photoURI.getAuthority());

                intentPic.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
                startActivityForResult(intentPic, CAMINO_FOTO);
            }
        }*/
    }

    private void closeTab() {
        if(mIsTwoPane){
            assert getFragmentManager() != null;
            getFragmentManager().beginTransaction().remove(this).commit();
        }
        else{
            Objects.requireNonNull(getActivity()).finish();
        }
        writeLog(TAG,"se cierra la ventana");
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        switch(requestCode){
            case CAMINO_FOTO:{
                if(resultCode == Activity.RESULT_OK){

                    mTexto.setText(getString(R.string.validacion_Foto));
                    /*mBitmapFinal = BitmapFactory.decodeFile(mCurrentPhotoPath);
                    File file = new File(mCurrentPhotoPath);
                    if(file.delete()) writeLog("CAMINO_FOTO", "SE BORRÓ LA IMAGEN DE LA RUTA TEMPORAL");
                    writeLog("CAMINO_FOTO", mBitmapFinal.getWidth()+ " " + mBitmapFinal.getHeight());

                    int dstWidth = (IMG_HEIGHT*mBitmapFinal.getWidth())/mBitmapFinal.getHeight();
                    mBitmapFinal = Bitmap.createScaledBitmap(
                            mBitmapFinal, dstWidth, IMG_HEIGHT, false
                    );
                    int puntoX = dstWidth/2 - IMG_WIDTH/2;
                    //int puntoY = IMG_HEIGHT_169/2 - IMG_HEIGHT/2;
                    /** ASD **/
                    //mBitmapFinal = Bitmap.createBitmap(mBitmapFinal, puntoX, 0, IMG_WIDTH, IMG_HEIGHT);

                    String imgPath = data.getStringExtra(ScanActivity.RESULT_IMAGE_PATH);
                    BitmapFactory.Options options = new BitmapFactory.Options();
                    options.inPreferredConfig = Bitmap.Config.ARGB_8888;
                    mBitmapFinal = BitmapFactory.decodeFile(imgPath, options);

                    mFoto.setImageBitmap(mBitmapFinal);
                    mFabDone.setEnabled(true);
                    mFabDone.setBackgroundColor(getResources().getColor(R.color.colorOk));
                    esRecaptura = true;
                }
                else if (resultCode == Activity.RESULT_CANCELED) {
                    writeLog("Error", "el codigo de respuesta fue 'cancelado'");
                }
                break;
            }
        }
    }//onActivityResult

    private File createImageFile() throws IOException {
        String imageFileName = "pic";
        File storageDir = new File(DIR_EXTERNA + DIR_REENROL + DIR_TMP);
        File image = File.createTempFile(
                imageFileName,  /* prefix */
                ".jpg",         /* suffix */
                storageDir      /* directory */
        );

        // Save a file: path for use with ACTION_VIEW intents
        mCurrentPhotoPath = image.getAbsolutePath();
        return image;
    }

    public interface OnFragmentInteractionListener {

        void onFragmentInteraction(HashMap<String, String> nombresDoctos, boolean isTwoPane);
    }

    public void onButtonPressed(HashMap<String,String> nombresDoctos, boolean isTwoPane) {
        if (mListener != null) {
            mListener.onFragmentInteraction(nombresDoctos, isTwoPane);
        }else
            writeLog("Error", "el listener es nulo en onButtonPressed");
    }

    private void procesoSubirImg(Bitmap img, String nome, int soapIdRef){
        //Se obtiene la data de la imagen
        byte[] imageData = tratarImagen(img);
        //Se genera el nombre de la imagen
        String nombre =
                guardarArchivo(nome, mFolioAfiSer, mFecha, mFolioAfi, mCurp, imageData, mTipoOp);
        //Se valida que se haya creado la imagen
        if(nombre.isEmpty())
            terminarAplicacion(getActivity(), ERROR_RESPONSE_DIR_NO_EXISTE);
        //Se agrega el nombre al arreglo de nombres
        HashMap<String,String> arr = new HashMap<>();
        arr.put(nome, nombre);
        onButtonPressed(arr, mIsTwoPane);
        //Se ejecuta el método para subir las imagenes al servidor
        soapSubirImgServidor(nombre, imageData, soapIdRef);
    }

    private void soapSubirImgServidor(String nombreImg, byte[] byteImg, int soapIdReferencia){
        //Hago el tratado de la imagen a base64
        String encodedImage = Base64.encodeToString(byteImg, Base64.NO_WRAP);

        SoapString sSoap = new SoapString(WS_MODULO_MOVIL,
                WS_METODO_SUBIR_IMG_SERVIDOR);
        sSoap.setParams(WS_PARAM_NOMBRE_IMAGEN, nombreImg);
        sSoap.setParams(WS_PARAM_IMAGEN_BASE64, encodedImage);

        CallWebService callWS = new CallWebService(this, soapIdReferencia);
        callWS.setFlag(true);
        callWS.execute(sSoap.getSoapString());
    }

    @Override
    public void onTaskComplete(String xmlMen, int id) {
        if(xmlMen.equals("")){
            writeLog(TAG,"Error al procesar la respuesta del servicio xmlMen está vacío");
            showInfoDialog(Objects.requireNonNull(getFragmentManager()),
                    this, DIALOG_TAG_ALERT_WS_FALLO);
            return;
        }
        Bundle values = getBundleValuesXML(xmlMen);
        if(values == null) {
            writeLog(TAG, "VALUES ES NULO EN onTaskComplete");
            showInfoDialog(Objects.requireNonNull(getFragmentManager()),
                    this, DIALOG_TAG_ALERT_WS_FALLO);
        }
        //Asigno los estatus a Variables para a continuación removerlos del Bundle
        int iEstatus = Integer.parseInt(Objects.requireNonNull(values).getString(WS_TAG_HTTP_ESTADO));
        String sMensaje = values.getString(WS_TAG_MSG);
        //Remuevo los valores de retorno del Bundle
        values.remove(WS_TAG_HTTP_ESTADO);
        values.remove(WS_TAG_MSG);

        switch(iEstatus){
            case RESP_HTTP_OK:
            case RESP_HTTP_OK_MOD:{
                switch (id){
                    case SOAP_SUBIR_IMG_SERVIDOR_FTRE: {
                        writeLog(TAG, "Se subió la fotografía correctamente");
                        break;
                    }
                    default:
                        writeLog(TAG,"HttpResponse: " + iEstatus + " idOp: " + id + ": " +
                                "Esto no debe de pasar en onTaskComplete switch id okWS");
                }
                break;
            }
            case RESP_HTTP_ERR_PARAM:
            case RESP_HTTP_ERR_ACCESO:
            case RESP_HTTP_ERR_NO_ENCONTRADO:
            case RESP_HTTP_ERR_SERVIDOR:{
                writeLog(TAG,"HttpResponse: " + iEstatus + " idOperación: " + id + ": " + sMensaje);
                if(id == SOAP_SUBIR_IMG_SERVIDOR_FTRE)
                    mDoctosNoSubidos.add(DOCTO_FOTO_REEN);
                else
                    writeLog(TAG,"HttpResponse: " + iEstatus + " idOp: " + id + ": " +
                            "Esto no debe de pasar en onTaskComplete else id errorWS");
                break;
            }
            default:
                writeLog(TAG,"HttpResponse: " + iEstatus + " idOp: " + id + ": " +
                        "Esto no debe de pasar en onTaskComplete switch wsResponse");
        }
    }

    @Override
    public void onDialogPositiveClick(int iTag) {
        switch (iTag){
            case DIALOG_TAG_CANCELAR_DIGI:{
                terminarAplicacion(Objects.requireNonNull(getActivity()), ERROR_RESPONSE_BTN_CANCEL);
                break;
            }
            case DIALOG_TAG_CONFIRM_RECAP:{
                capturarFotoTrab();
                break;
            }
            case DIALOG_TAG_CONFIRM:{
                aceptarImagen();
                break;
            }
            case DIALOG_TAG_ALERT_CAPTURA_FOTO:{
                writeLog(TAG, "El usuario trató te guardar la foto sin capturarla previamente");
                break;
            }
            case DIALOG_TAG_ALERT_WS_FALLO:{
                terminarAplicacion(Objects.requireNonNull(getActivity()), ERROR_RESPONSE_WS_FAILED);
                break;
            }
            case DIALOG_TAG_ALERT_EMPTY_PARAM:{
                terminarAplicacion(Objects.requireNonNull(getActivity()), ERROR_RESPONSE_EMPTY_PARAMS);
                break;
            }
        }
    }

    @Override
    public void onDialogNegativeClick(int iTag) {
        switch (iTag){
            case DIALOG_TAG_CANCELAR_DIGI:{
                writeLog(TAG, "se cancela cancelar digitalización");
                break;
            }
            case DIALOG_TAG_CONFIRM_RECAP:{
                writeLog(TAG, "se cancela cofirmar recaptura");
                break;
            }
            case DIALOG_TAG_CONFIRM:{
                writeLog(TAG, "se cancela la confirmación del término del proceso de INE");
                break;
            }
            default:
                writeLog(TAG, "cancelo algo no identificado");
        }
    }

    public void aceptarImagen(){
        mEsPrimeraVez = true;

        DoctoListFragment.incrementarIndexados();

        procesoSubirImg(mBitmapFinal, docto.getNomenclatura(), SOAP_SUBIR_IMG_SERVIDOR_FTRE);
        //cambiarIcono(mLvDoctos.getChildAt(2), R.drawable.ic_pic_check);

        if(docto.getIcono() == R.drawable.ic_file){

            cambiarIcono(mLvDoctos.getChildAt(docto.getPosicion()), R.drawable.ic_file_check);
        }
        else if(docto.getIcono() == R.drawable.ic_pic){

            writeLog(TAG, "2 " + docto.getPosicion());

            cambiarIcono(mLvDoctos.getChildAt(docto.getPosicion()), R.drawable.ic_pic_check);
        }
        else if(docto.getIcono() == R.drawable.ic_ine){

            writeLog(TAG, "3 " + docto.getPosicion());
            cambiarIcono(mLvDoctos.getChildAt(docto.getPosicion()), R.drawable.ic_id_check);
        }

        closeTab();
    }
}
