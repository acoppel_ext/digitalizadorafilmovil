package com.aforecoppelafil.my.digitalizadorafilmovil.pojo;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Sergio Gil (AP Interfaces) on 14/12/18.
 */

public class DocComprobante {

    @SerializedName("iFolio")
    public String iFolio;

    //creates a json-format string
    public static String createDocComprobante(String iFolio){
        String info = "{\"iFolio\":\""+iFolio+"\"}";


        return info;
    }
}
