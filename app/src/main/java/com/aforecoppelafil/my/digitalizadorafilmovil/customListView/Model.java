package com.aforecoppelafil.my.digitalizadorafilmovil.customListView;

public class Model{

    private int icon;
    private String title;

    private boolean isGroupHeader = false;

    public Model(String title) {
        this(-1,title);
        isGroupHeader = true;
    }
    public Model(int icon, String title) {
        super();
        this.icon = icon;
        this.title = title;
    }

    public int getIcon() {
        return icon;
    }

    public void setIcon(int icon) {
        this.icon = icon;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
