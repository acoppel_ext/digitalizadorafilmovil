package com.aforecoppelafil.my.digitalizadorafilmovil.pojo;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Sergio Gil (AP Interfaces) on 16/04/19.
 */

public class DoctoCompDom {

    @SerializedName("estatus")
    public Integer estatus;
    @SerializedName("descripcion")
    public String descripcion;
    @SerializedName("registros")
    public List<registros> data = new ArrayList<>();
    public class registros {

        @SerializedName("respuesta")
        public Integer respuesta;


    }
}
