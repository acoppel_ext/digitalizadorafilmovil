package com.aforecoppelafil.my.digitalizadorafilmovil;

import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;

import com.aforecoppelafil.my.digitalizadorafilmovil.R;

public class MainDetailActivity extends AppCompatActivity {

    DoctoDetailFragment mFragmentDoctoDetail;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_detail);
        // Fetch the item to display from bundle
       // Documento docto = getIntent().getParcelableExtra("docto");
        //boolean isTwoPane = getIntent().getBooleanExtra("tipoPantalla",false);
        if (savedInstanceState == null) {
            mFragmentDoctoDetail = DoctoDetailFragment.newInstance(getIntent().getExtras());
            // Insert detail fragment based on the item passed
//            mFragmentDoctoDetail = new DoctoDetailFragment();
            FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
            ft.replace(R.id.flDetailContainer, mFragmentDoctoDetail);
            ft.commit();
        }

    }
}
