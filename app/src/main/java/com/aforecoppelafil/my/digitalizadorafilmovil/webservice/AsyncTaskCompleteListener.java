package com.aforecoppelafil.my.digitalizadorafilmovil.webservice;

public interface AsyncTaskCompleteListener<T> {

    void onTaskComplete(T result, int id);

}

