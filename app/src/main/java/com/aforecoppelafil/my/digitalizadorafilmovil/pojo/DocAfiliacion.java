package com.aforecoppelafil.my.digitalizadorafilmovil.pojo;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Sergio Gil (AP Interfaces) on 14/12/18.
 */

public class DocAfiliacion {

    @SerializedName("estatus")
    public Integer estatus;
    @SerializedName("descripcion")
    public String descripcion;
    @SerializedName("registros")
    public List<registros> data = new ArrayList<>();

    public class registros {

        @SerializedName("identificador")
        public String identificador;
        @SerializedName("descripcion")
        public String descripcion;
        @SerializedName("iddocumento")
        public String iddocumento;

    }
}
