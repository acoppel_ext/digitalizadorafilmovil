package com.aforecoppelafil.my.digitalizadorafilmovil;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.widget.AppCompatImageView;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.aforecoppelafil.my.digitalizadorafilmovil.documento.Documento;
import com.aforecoppelafil.my.digitalizadorafilmovil.webservice.AsyncTaskCompleteListener;
import com.aforecoppelafil.my.digitalizadorafilmovil.webservice.CallWebService;
import com.aforecoppelafil.my.digitalizadorafilmovil.webservice.SoapString;
import com.aforecoppelafil.my.digitalizadorafilmovil.R;
import com.scanlibrary.ScanActivity;

import java.util.HashMap;
import java.util.Objects;

import ru.dimorinny.floatingtextbutton.FloatingTextButton;

import static com.aforecoppelafil.my.digitalizadorafilmovil.DoctoListFragment.mEsPrimeraVez;
import static com.aforecoppelafil.my.digitalizadorafilmovil.DoctoListFragment.mLvDoctos;

public class DoctoDetailFragment extends Fragment implements AsyncTaskCompleteListener<String>,
        Utilerias.NoticeDialogFragment.NoticeDialogListener,
        Utilerias.InfoDialogFragment.NoticeDialogListener{

    private static final int CAMINO_ANVERSO = 55;
    private static final int CAMINO_REVERSO = 66;
    private static final int A4_WIDTH = 2544;
    private static final int A4_HEIGHT = 3300;
    private static final int A4_REVERSO_POS_LETF = 400;
    private static final int A4_REVERSO_POS_TOP = 1237;
    private static final String TAG = "DoctoDetailFragment";

    private boolean esAnverso = true;
    private boolean esReverso = false;
    private boolean esRecaptura = false;
    private boolean mIsTwoPane;

    private String mFolioAfi;
    private String mFolioAfiSer;
    private String mCurp;
    private String mFecha;
    private String mTipoOp;
    private String iTabla;
    private Documento mDocto;

    private Bitmap mBitmapFinal;
    private TextView mTexto;
    private FloatingTextButton mFabDone;
    private AppCompatImageView mFoto;
    private String EstatusLog = "Digitalizadormovil - DoctoDetailFragment Version:"+ BuildConfig.VERSION_NAME;
    private HashMap<String, String> mNombresDoctos;

    private String MensajeLog = "";

    private OnFragmentInteractionListener mListener;
    private Documento docto;
    private String CompDom;

    public DoctoDetailFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param args Parameter 1.
     * @return A new instance of fragment DoctoDetailFragment.
     */
    public static DoctoDetailFragment newInstance(Bundle args) {
        DoctoDetailFragment fragment = new DoctoDetailFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mCurp        = getArguments().getString(Utilerias.TAG_RESP_CURP);
            mFecha       = getArguments().getString(Utilerias.TAG_RESP_FECHA);
            mDocto       = getArguments().getParcelable(Utilerias.PARAM_DOCTO);
            mTipoOp      = getArguments().getString(Utilerias.TAG_RESP_TIPO_SOL);
            iTabla      = getArguments().getString(Utilerias.PARAM_OPCION_TRAMITE);
            mFolioAfi    = getArguments().getString(Utilerias.PARAM_FOLIO_AFI);
            mIsTwoPane   = getArguments().getBoolean(Utilerias.PARAM_DOS_PANELES);
            mFolioAfiSer = getArguments().getString(Utilerias.TAG_RESP_FOLIO);
            docto        = getArguments().getParcelable(Utilerias.PARAM_DOCTO);
            CompDom        = getArguments().getString("comprobante_domicilio");
        }
        else{
            Utilerias.writeLog("Error","getArgumentes está vacío");
            Utilerias.showInfoDialog(Objects.requireNonNull(getFragmentManager()),
                    this, Utilerias.DIALOG_TAG_ALERT_EMPTY_PARAM);
        }
        mNombresDoctos = new HashMap<>();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_docto_detail,
                container, false);
        mFabDone = view.findViewById(R.id.fab_done);
        mFabDone.setEnabled(false);
        mFabDone.setOnClickListener(viu -> onClickGral(R.id.fab_done));
        view.findViewById(R.id.fab_cancel).setOnClickListener(viu -> onClickGral(R.id.fab_cancel));
        view.findViewById(R.id.fab_picture).setOnClickListener(viu -> onClickGral(R.id.fab_picture));

        mFoto = view.findViewById(R.id.ivFoto);
        mTexto = view.findViewById(R.id.tvInstrucciones);
        String instruc = "";
        switch(mDocto.getNomenclatura()){
            case Utilerias.DOCTO_FORMATO_REEN:{
                instruc = getString(R.string.item_instruc_FARE);
                break;
            }
            case Utilerias.DOCTO_ID_REEN_ANV:{
                instruc = getString(R.string.item_instruc_IDRA);
                break;
            }
            case Utilerias.DOCTO_ID_REEN_REV:{
                instruc = getString(R.string.item_instruc_IDRR);
                break;
            }
            case Utilerias.DOCTO_ID_REEN:{
                instruc = getString(R.string.item_instruc_IDRA);
                break;
            }
            default:{
                instruc = getString(R.string.item_instruc_ID);
                break;
            }
        }
        mTexto.setText(instruc);
        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof DoctoDetailFragment.OnFragmentInteractionListener) {
            mListener = (DoctoDetailFragment.OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public void onClickGral(int id) {
        switch (id){
            case R.id.fab_picture:{
                if(esRecaptura && (esAnverso || esReverso))
                    /*Utilerias.showNoticeDialog(Objects.requireNonNull(getFragmentManager()),
                            this, Utilerias.DIALOG_TAG_CONFIRM_RECAP);*/
                    digitalizarDocto();
                else if(!esReverso && !esAnverso)
                    /*Utilerias.showNoticeDialog(Objects.requireNonNull(getFragmentManager()),
                            this, Utilerias.DIALOG_TAG_CONFIRM_RESTART_IDRE);*/
                    recapturarImagenes();
                else
                    digitalizarDocto();

                break;
            }
            case R.id.fab_done:{
                if(mFabDone.isEnabled())
                    if(mBitmapFinal == null){
                        Utilerias.showInfoDialog(Objects.requireNonNull(getFragmentManager()),
                                this, Utilerias.DIALOG_TAG_ALERT_CAPTURA);
                    }else{
                        /*Utilerias.showNoticeDialog(Objects.requireNonNull(getFragmentManager()),
                                this, Utilerias.DIALOG_TAG_CONFIRM);*/
                        aceptarImagen();
                    }

                break;
            }
            case R.id.fab_cancel:{
//                closeTab();
                Utilerias.showNoticeDialog(Objects.requireNonNull(getFragmentManager()),
                        this, Utilerias.DIALOG_TAG_CANCELAR_DIGI);
                break;
            }
            default:
                Utilerias.writeLog("Error", "entró en el campo default del onClick " + id);
                Utilerias.writeLog("Error", "picture " + R.id.fab_picture + " done " +
                        R.id.fab_done + " cancel " + R.id.fab_cancel);
        }
    }//onClick

    private void digitalizarDocto() {
        Intent intent = new Intent(getContext(), ScanActivity.class);
        // Set title color - optional
        intent.putExtra("TipoDocumento", 1);
        intent.putExtra(ScanActivity.EXTRA_ACTION_BAR_COLOR, R.color.colorPrimaryDark);
        // Set language - optional
        intent.putExtra(ScanActivity.EXTRA_LANGUAGE, "es");
        startActivityForResult(intent, esAnverso ? CAMINO_ANVERSO : CAMINO_REVERSO);
    }

    private void closeTab() {
        if(mIsTwoPane){
            assert getFragmentManager() != null;
            getFragmentManager().beginTransaction().remove(this).commit();
        }
        else{
            Objects.requireNonNull(getActivity()).finish();
        }
        Utilerias.writeLog(TAG,"se cierra la ventana");
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        switch(requestCode){
            case CAMINO_ANVERSO:{
                if(resultCode == Activity.RESULT_OK){
                    mTexto.setText(getString(R.string.validacion_Anverso));
                    String imgPath = data.getStringExtra(ScanActivity.RESULT_IMAGE_PATH);
                    BitmapFactory.Options options = new BitmapFactory.Options();
                    options.inPreferredConfig = Bitmap.Config.ARGB_8888;
                    mBitmapFinal = BitmapFactory.decodeFile(imgPath, options);
                    mFoto.setImageBitmap(mBitmapFinal);
                    mFabDone.setEnabled(true);
                    mFabDone.setBackgroundColor(getResources().getColor(R.color.colorOk));
                    esRecaptura = true;
                }
                else if (resultCode == Activity.RESULT_CANCELED) {
                    Utilerias.writeLog("Error", "el codigo de respuesta fue 'cancelado'");
                }
                break;
            }
            case CAMINO_REVERSO:{
                if(resultCode == Activity.RESULT_OK){
                    mTexto.setText(getString(R.string.validacion_Reverso));
                    String imgPath = data.getStringExtra(ScanActivity.RESULT_IMAGE_PATH);
                    BitmapFactory.Options options = new BitmapFactory.Options();
                    options.inPreferredConfig = Bitmap.Config.ARGB_8888;
                    Bitmap bitmap = BitmapFactory.decodeFile(imgPath, options);
                    mFoto.setImageBitmap(bitmap);

                    //Una vez aceptadas las imagenes de anverso y reverso, se procede a subirlas
                    //Primero Anverso
                    Utilerias.writeLog(" ", "iTabla " + iTabla);
                    if(Integer.parseInt(iTabla) == 8){
                        Utilerias.writeLog("Error", "iTabla " + iTabla);
                        procesoSubirImg(mBitmapFinal, Utilerias.DOCTO_ID_REEN_ANV, Utilerias.SOAP_SUBIR_IMG_SERVIDOR_IDRA);
                        //Luego el Reverso
                        procesoSubirImg(bitmap, Utilerias.DOCTO_ID_REEN_REV, Utilerias.SOAP_SUBIR_IMG_SERVIDOR_IDRR);
                    }else{
                        Utilerias.writeLog("Error", "iTabla " + iTabla);
                        procesoSubirImg(mBitmapFinal, Utilerias.DOCTO_ID_EN_ANV, Utilerias.SOAP_SUBIR_IMG_SERVIDOR_IDRA);
                        //Luego el Reverso
                        procesoSubirImg(bitmap, Utilerias.DOCTO_ID_EN_REV, Utilerias.SOAP_SUBIR_IMG_SERVIDOR_IDRR);
                    }
                    //Se fusiona el anverso y reverso
                    mBitmapFinal = fusionImagenes(mBitmapFinal, bitmap);
                    esRecaptura = true;
                    mFabDone.setEnabled(true);
                    mFabDone.setBackgroundColor(getResources().getColor(R.color.colorOk));
                }
                else if (resultCode == Activity.RESULT_CANCELED) {
                    Utilerias.writeLog("Error", "el codigo de respuesta fue 'cancelado'");
                }
                break;
            }
        }
    }//onActivityResult

    public Bitmap fusionImagenes(Bitmap bmp1, Bitmap bmp2) {
        Bitmap bmOverlay = Bitmap.createBitmap(A4_WIDTH, A4_HEIGHT, bmp1.getConfig());
        Canvas canvas = new Canvas(bmOverlay);
        canvas.drawColor(Color.WHITE);
        canvas.drawBitmap(bmp1, new Matrix(), null);
        canvas.drawBitmap(bmp2, A4_REVERSO_POS_LETF, A4_REVERSO_POS_TOP, null);

        return bmOverlay;
    }

    private void procesoSubirImg(Bitmap img, String nome, int soapIdRef){
        Utilerias.writeLog("procesoSubirImg", "nome " + nome);
        //Se obtiene la data de la imagen
        byte[] imageData = Utilerias.tratarImagen(img);
        //Se genera el nombre de la imagen
        String nombre =
                Utilerias.guardarArchivo(nome, mFolioAfiSer, mFecha, mFolioAfi, mCurp, imageData, mTipoOp);
        //Se valida que se haya creado la imagen
        if(nombre.isEmpty())
            Utilerias.terminarAplicacion(getActivity(), Utilerias.ERROR_RESPONSE_DIR_NO_EXISTE);
        //Se agrega el nombre al arreglo de nombres
        mNombresDoctos.put(nome, nombre);
        //Se ejecuta el método para subir las imagenes al servidor
        soapSubirImgServidor(nombre, imageData, soapIdRef);
    }

    public interface OnFragmentInteractionListener {

        void onFragmentInteraction(HashMap<String,String> nombresDoctos, boolean isTwoPane);


    }

    public void onButtonPressed(HashMap<String,String> nombresDoctos, boolean isTwoPane) {
        if (mListener != null) {
            mListener.onFragmentInteraction(nombresDoctos, isTwoPane);
        }else
            Utilerias.writeLog("Error", "el listener es nulo en onButtonPressed");
    }

    private void soapSubirImgServidor(String nombreImg, byte[] byteImg, int soapIdReferencia){
        //Hago el tratado de la imagen a base64
        String encodedImage = Base64.encodeToString(byteImg, Base64.NO_WRAP);

        SoapString sSoap = new SoapString(Utilerias.WS_MODULO_MOVIL,
                Utilerias.WS_METODO_SUBIR_IMG_SERVIDOR);
        sSoap.setParams(Utilerias.WS_PARAM_NOMBRE_IMAGEN, nombreImg);
        sSoap.setParams(Utilerias.WS_PARAM_IMAGEN_BASE64, encodedImage);


        CallWebService callWS = new CallWebService(this, soapIdReferencia);
        callWS.setFlag(true);
        callWS.execute(sSoap.getSoapString());
    }

    private void soapGuardarLogs(){
        SoapString sSoap = new SoapString("wsafiliacionmovil",
                "GuardarLogs");

        sSoap.setParams("Mensaje", MensajeLog);
        sSoap.setParams("Estatus", EstatusLog);

        CallWebService callWS = new CallWebService(null, 0);
        callWS.setFlag(false);
        callWS.execute(sSoap.getSoapString());
    }

    @Override
    public void onTaskComplete(String xmlMen, int id) {
        if(xmlMen.equals("")){
//            showToast(getActivity(),"Error al procesar la respuesta del servicio");
            Utilerias.writeLog(TAG,"Error al procesar la respuesta del servicio xmlMen está vacío");
            Utilerias.showInfoDialog(Objects.requireNonNull(getFragmentManager()),
                    this, Utilerias.DIALOG_TAG_ALERT_WS_FALLO);
            return;
        }
//        HashMap<String, String> values = getValuesXML(xmlMen);
        Bundle values = Utilerias.getBundleValuesXML(xmlMen);
        if(values == null) {
            Utilerias.writeLog(TAG, "VALUES ES NULO EN onTaskComplete");
            Utilerias.showInfoDialog(Objects.requireNonNull(getFragmentManager()),
                    this, Utilerias.DIALOG_TAG_ALERT_WS_FALLO);
        }
        //Asigno los estatus a Variables para a continuación removerlos del Bundle
        int iEstatus = Integer.parseInt(Objects.requireNonNull(values).getString(Utilerias.WS_TAG_HTTP_ESTADO));
        String sMensaje = values.getString(Utilerias.WS_TAG_MSG);
        //Remuevo los valores de retorno del HashMap
        values.remove(Utilerias.WS_TAG_HTTP_ESTADO);
        values.remove(Utilerias.WS_TAG_MSG);

        switch(iEstatus){
            case Utilerias.RESP_HTTP_OK:
            case Utilerias.RESP_HTTP_OK_MOD:{
                switch (id){
                    case Utilerias.SOAP_SUBIR_IMG_SERVIDOR_IDRE: {
                        Utilerias.writeLog(TAG,  "Se subió el IDRE correctamente");
                        break;
                    }
                    case Utilerias.SOAP_SUBIR_IMG_SERVIDOR_IDRA: {
                        Utilerias.writeLog(TAG,  "Se subió el IDRA correctamente");
                        break;
                    }
                    case Utilerias.SOAP_SUBIR_IMG_SERVIDOR_IDRR: {
                        Utilerias.writeLog(TAG,  "Se subió el IDRR correctamente");
                        break;
                    }
                    default:
                        Utilerias.writeLog(TAG,"HttpResponse: " + iEstatus + " idOp: " + id + ": " +
                                "Esto no debe de pasar en onTaskComplete switch id okWS");
                }
                break;
            }
            case Utilerias.RESP_HTTP_ERR_PARAM:
            case Utilerias.RESP_HTTP_ERR_ACCESO:
            case Utilerias.RESP_HTTP_ERR_NO_ENCONTRADO:
            case Utilerias.RESP_HTTP_ERR_SERVIDOR:{
                MensajeLog = "iEstatus: "+iEstatus+"| id: "+id;
                soapGuardarLogs();
                MensajeLog = "CURP: "+mCurp + "|FECHA: "+mFecha +"|TIPO_OPERACION: "+ mTipoOp +"|FOLIO_AFI: "+mFolioAfi +"|FOLIO_AFISER: "+ mFolioAfiSer;
                soapGuardarLogs();
                switch (id){
                    case Utilerias.SOAP_SUBIR_IMG_SERVIDOR_IDRE: {
                        Utilerias.writeLog(TAG,  "No se subió el IDRE correctamente");
                        MainListActivity.mDoctosNoSubidos.add(Utilerias.DOCTO_ID_REEN);
                        break;
                    }
                    case Utilerias.SOAP_SUBIR_IMG_SERVIDOR_IDRA: {
                        Utilerias.writeLog(TAG,  "No se subió el IDRA correctamente");
                        MainListActivity.mDoctosNoSubidos.add(Utilerias.DOCTO_ID_REEN_ANV);
                        break;
                    }
                    case Utilerias.SOAP_SUBIR_IMG_SERVIDOR_IDRR: {
                        Utilerias.writeLog(TAG,  "No se subió el IDRR correctamente");
                        MainListActivity.mDoctosNoSubidos.add(Utilerias.DOCTO_ID_REEN_REV);
                        break;
                    }
                    default:
                        Utilerias.writeLog(TAG,"HttpResponse: " + iEstatus + " idOp: " + id + ": " +
                                "Esto no debe de pasar en onTaskComplete switch id errorWS");
                }
                Utilerias.writeLog(TAG,"HttpResponse: " + iEstatus + " idOperación: " + id + ": " + sMensaje);
                break;
            }
            default:
                Utilerias.writeLog(TAG,"HttpResponse: " + iEstatus + " idOp: " + id + ": " +
                        "Esto no debe de pasar en onTaskComplete switch wsResponse");
        }
    }

    @Override
    public void onDialogPositiveClick(int iTag) {
        switch (iTag){
            case Utilerias.DIALOG_TAG_CONFIRM:{
                aceptarImagen();
                break;
            }
            case Utilerias.DIALOG_TAG_CONFIRM_RECAP:{
                digitalizarDocto();
                break;
            }
            case Utilerias.DIALOG_TAG_CONFIRM_RESTART_IDRE:{
                recapturarImagenes();
                break;
            }
            case Utilerias.DIALOG_TAG_CANCELAR_DIGI:{
                Utilerias.terminarAplicacion(Objects.requireNonNull(getActivity()), Utilerias.ERROR_RESPONSE_BTN_CANCEL);
                break;
            }
            case Utilerias.DIALOG_TAG_ALERT_WS_FALLO:{
                Utilerias.terminarAplicacion(Objects.requireNonNull(getActivity()), Utilerias.ERROR_RESPONSE_WS_FAILED);
                break;
            }
            case Utilerias.DIALOG_TAG_ALERT_EMPTY_PARAM:{
                Utilerias.terminarAplicacion(Objects.requireNonNull(getActivity()), Utilerias.ERROR_RESPONSE_EMPTY_PARAMS);
                break;
            }
        }
    }

    @Override
    public void onDialogNegativeClick(int iTag) {
        switch (iTag){
            case Utilerias.DIALOG_TAG_CONFIRM:{
                Utilerias.writeLog(TAG, "se presionó cancelar en el dialog confirm");
                break;
            }
            case Utilerias.DIALOG_TAG_CANCELAR_DIGI:{
                Utilerias.writeLog(TAG, "se presionó cancelar en calcelar digitalizacion");
                break;
            }
            case Utilerias.DIALOG_TAG_CONFIRM_RECAP:{
                Utilerias.writeLog(TAG, "se presionó cancelar al confirmar la recaptura");
                break;
            }
            case Utilerias.DIALOG_TAG_CONFIRM_RESTART_IDRE:{
                Utilerias.writeLog(TAG, "se presionó cancelar al confirmar la recaptura desde el inicio");
                break;
            }
            default:
                Utilerias.writeLog(TAG, "cancelo algo no identificado");
        }
    }

    public void aceptarImagen(){
        if(esAnverso){
            mTexto.setText(R.string.item_instruc_IDRR);

            mFoto.setImageDrawable(
                    Objects.requireNonNull(getActivity()).
                            getDrawable(R.drawable.ine_reverso)
            );
            mFabDone.setEnabled(false);
            mFabDone.setBackgroundColor(getResources().getColor(R.color.colorDisabled));
            esRecaptura = false;
            esAnverso = false;
            esReverso = true;
        }
        else if(esReverso){
            mTexto.setText(R.string.validacion_Identificacion);
            mFoto.setImageBitmap(mBitmapFinal);
            esRecaptura = false;
            esReverso = false;
        }else{
            mEsPrimeraVez = true;

            DoctoListFragment.incrementarIndexados();

            if(Integer.parseInt(iTabla) == 8){
                procesoSubirImg(mBitmapFinal, Utilerias.DOCTO_ID_REEN, Utilerias.SOAP_SUBIR_IMG_SERVIDOR_IDRE);
            }else{
                procesoSubirImg(mBitmapFinal, Utilerias.DOCTO_ID_EN, Utilerias.SOAP_SUBIR_IMG_SERVIDOR_IDRE);
            }

            //cambiarIcono(mLvDoctos.getChildAt(0), R.drawable.ic_id_check);
            if(docto.getIcono() == R.drawable.ic_file){
                Utilerias.cambiarIcono(mLvDoctos.getChildAt(docto.getPosicion()), R.drawable.ic_file_check);
            }
            else if(docto.getIcono() == R.drawable.ic_pic){
                Utilerias.cambiarIcono(mLvDoctos.getChildAt(docto.getPosicion()), R.drawable.ic_pic_check);
            }
            else if(docto.getIcono() == R.drawable.ic_ine){
                Utilerias.cambiarIcono(mLvDoctos.getChildAt(docto.getPosicion()), R.drawable.ic_id_check);
            }


            if( Integer.parseInt(CompDom) == 1 && mDocto.getNomenclatura().substring(0,2).equals("ID")){
                procesoSubirImg(mBitmapFinal,"DO00", Utilerias.SOAP_SUBIR_IMG_SERVIDOR_DO00);
                Utilerias.cambiarIcono(mLvDoctos.getChildAt(0), R.drawable.ic_id_check);
                Utilerias.cambiarIcono(mLvDoctos.getChildAt(2), R.drawable.ic_id_check);
            }

            onButtonPressed(mNombresDoctos, mIsTwoPane);
            closeTab();
        }
    }

    public void recapturarImagenes(){
        mTexto.setText(R.string.item_instruc_IDRA);
        mFoto.setImageDrawable(
                Objects.requireNonNull(getActivity()).getDrawable(R.drawable.ine_anverso)
        );
        mFabDone.setEnabled(false);
        mFabDone.setBackgroundColor(getResources().getColor(R.color.colorDisabled));
        esAnverso = true;
        esReverso = false;
        esRecaptura = false;
    }
}


