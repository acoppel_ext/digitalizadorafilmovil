package com.aforecoppelafil.my.digitalizadorafilmovil.pojo;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Sergio Gil (AP Interfaces) on 14/12/18.
 */

public class Indexados {

    @SerializedName("iOpcion")
    public String iOpcion;
    @SerializedName("iFolioAfiSer")
    public String iFolioAfiSer;
    @SerializedName("cNomenclatura")
    public String cNomenclatura;
    @SerializedName("iTabla")
    public String iTabla;

    public Indexados(String iOpcion, String iFolioAfiSer, String cNomenclatura, String iTabla) {
        this.iOpcion = iOpcion;
        this.iFolioAfiSer = iFolioAfiSer;
        this.cNomenclatura = cNomenclatura;
        this.iTabla = iTabla;
    }

    //creates a json-format string
    public static String createIndexados(String iOpcion, String iFolioAfiSer, String cNomenclatura, String iTabla){
        String info = "{\"iOpcion\":\""+iOpcion+"\",\"iFolioAfiSer\":\""+iFolioAfiSer+"\",\"cNomenclatura\":\""+cNomenclatura+"\",\"iTabla\":\""+iTabla+"\"}";


        return info;
    }
}
