package com.aforecoppelafil.my.digitalizadorafilmovil.webservice;

import com.aforecoppelafil.my.digitalizadorafilmovil.BuildConfig;

import java.util.HashMap;
import java.util.Map;

import static com.aforecoppelafil.my.digitalizadorafilmovil.Utilerias.WS_PARAM_TOKEN_ACCESS;
import static com.aforecoppelafil.my.digitalizadorafilmovil.Utilerias.WS_PARAM_TOKEN_APP;
import static com.aforecoppelafil.my.digitalizadorafilmovil.Utilerias.getTokenAcces;

public class SoapString {

    private String SoapString;
    private String Namespace;
    private String MethodName;
    private static final String apiKeyModulo = getTokenAcces(BuildConfig.ApiKeyModulo, true);
    private static final String apiKeyApp = getTokenAcces(BuildConfig.ApiKeyDigi, false);
    private HashMap<String, String> Params;

    public SoapString(String Namespace, String MethodName){
        this.Namespace = Namespace;
        this.MethodName = MethodName;
        Params = new HashMap<>();
        setParams(WS_PARAM_TOKEN_ACCESS, apiKeyModulo);
        setParams(WS_PARAM_TOKEN_APP, apiKeyApp);
        setParams("cVersion","Digitalizadormovil Version:"+ BuildConfig.VERSION_NAME);
    }

    private void generateRequest(){

        SoapString = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"no\"?>" +
                "<soapenv:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" " +
                "xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" " +
                "xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" " +
                "xmlns:urn=\"urn:" + Namespace + "\">\n" +
                "<soapenv:Header/>\n<soapenv:Body>\n" +
                "<urn:" + MethodName + " soapenv:encodingStyle=\"http://schemas.xmlsoap.org/soap/encoding/\">\n" +
                "<inParam>\n" +
                getStringParams() +
                "</inParam>\n" +
                "</urn:" + MethodName + ">\n" +
                "</soapenv:Body></soapenv:Envelope>";

    }

    private StringBuilder getStringParams(){

        StringBuilder tempParams = new StringBuilder();
        String param;

        for (Map.Entry<String, String> entry : Params.entrySet()) {
            param = "<" + entry.getKey() + ">" + entry.getValue() + "</" + entry.getKey() + ">\n";
            tempParams.append(param);
        }

        return tempParams;
    }

    public String getSoapString(){
        generateRequest();
        return SoapString;
    }

    public void setParams(String key, String value) {
        this.Params.put(key,value);
    }

    public String getNamespace() {
        return Namespace;
    }

    public String getMethodName() {
        return MethodName;
    }

    public HashMap<String, String> getParams() {
        return Params;
    }



}
