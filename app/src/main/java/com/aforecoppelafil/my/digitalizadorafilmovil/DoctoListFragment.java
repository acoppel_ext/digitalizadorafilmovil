package com.aforecoppelafil.my.digitalizadorafilmovil;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.TextInputEditText;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.aforecoppelafil.my.digitalizadorafilmovil.documento.DoctoAdapter;
import com.aforecoppelafil.my.digitalizadorafilmovil.documento.Documento;
import com.aforecoppelafil.my.digitalizadorafilmovil.pojo.DocComprobante;
import com.aforecoppelafil.my.digitalizadorafilmovil.pojo.DoctosCompDom;
import com.aforecoppelafil.my.digitalizadorafilmovil.pojo.DoctosIndexados;
import com.aforecoppelafil.my.digitalizadorafilmovil.pojo.Indexados;
import com.aforecoppelafil.my.digitalizadorafilmovil.webservice.AsyncTaskCompleteListener;
import com.aforecoppelafil.my.digitalizadorafilmovil.webservice.CallWebService;
import com.aforecoppelafil.my.digitalizadorafilmovil.webservice.SoapString;
import com.aforecoppelafil.my.digitalizadorafilmovil.R;

import java.util.ArrayList;
import java.util.Objects;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link DoctoListFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link DoctoListFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class DoctoListFragment extends Fragment implements AsyncTaskCompleteListener<String>,
        Utilerias.NoticeDialogFragment.NoticeDialogListener,
        Utilerias.InfoDialogFragment.NoticeDialogListener {

    APIInterface apiInterface;

    private static final String TAG = "DoctoListFragment";

    private Bundle mInfoTrab;

    private OnFragmentInteractionListener mListener;

    public static DoctoAdapter mAdapterDoctos;
    public static ListView mLvDoctos;
    private String EstatusLog = "Digitalizadormovil - DoctoListFragment Version:"+ BuildConfig.VERSION_NAME;
    private String MensajeLog = "";
    private int mPosicion;
    private String mFolio;
    private String ipServidor;
    public String sNomenclatura;
    private String ipAddress;
    private String mTipoOperacion;
    public static boolean mEsPrimeraVez = true ;
    public String iTabla;
    public int posicionDocto = 0;
    public int iResp = 0;
    private int iContador=0;
    public int iComprobante=0;
    public String CompDom = "0";
    public String Nomenclatura = "";

    public static int iContDoc=0;
    public static int iDocTerm=0;
    public static int iDocCap=0;

    public View viu;
    public DoctoListFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param inTrab Parameter 1.
     * @return A new instance of fragment DoctoListFragment.
     */
    public static DoctoListFragment newInstance(Bundle inTrab) {
        DoctoListFragment fragment = new DoctoListFragment();

        fragment.setArguments(inTrab);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            Bundle bunParamsEntrada = getArguments();
            mTipoOperacion  = bunParamsEntrada.getString(Utilerias.PARAM_TIPO_OPERACION);
            mFolio          = bunParamsEntrada.getString(Utilerias.PARAM_FOLIO_AFILIACION);
            iTabla          = bunParamsEntrada.getString("OPCION_TRAMITE");
            ipAddress       = bunParamsEntrada.getString(Utilerias.PARAM_SERVIDOR);
            if(mTipoOperacion.equals(Utilerias.OPCION_SERVICIOS))
                mFolio      = bunParamsEntrada.getString(Utilerias.PARAM_FOLIO_SERVICIO);
            mPosicion = -1;
            //Se ejecuta el servicio para obtener la información del trabajador
            soapObtenerInfo(mFolio, mTipoOperacion);
        }
        else{
            Utilerias.writeLog("Error","getArgumentes está vacío");
            Utilerias.showInfoDialog(Objects.requireNonNull(getFragmentManager()),
                    this, Utilerias.DIALOG_TAG_ALERT_EMPTY_PARAM);
        }

    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate view
        View view = inflater.inflate(R.layout.fragment_docto_list,
                container, false);
//        setInfoValues(view);
        // Attach adapter to listView}
        viu = inflater.inflate(R.layout.fragment_main, container, false);
        TextView tvInstrucciones = viu.findViewById(R.id.tvInstrucciones);
        mLvDoctos = view.findViewById(R.id.lvItems);
        return view;
    }

    public static void setListViewHeightBasedOnChildren(ListView listView) {

        ListAdapter listAdapter = listView.getAdapter();
        if (listAdapter == null) return;

        int desiredWidth = View.MeasureSpec.makeMeasureSpec(listView.getWidth(),
                View.MeasureSpec.UNSPECIFIED);
        int totalHeight = 0;
        View view = null;

        for (int i = 0; i < listAdapter.getCount(); i++) {
            view = listAdapter.getView(i, view, listView);
            if (i == 0)
                view.setLayoutParams(new ViewGroup.LayoutParams(desiredWidth,
                    ViewGroup.LayoutParams.WRAP_CONTENT));
            view.measure(desiredWidth, View.MeasureSpec.UNSPECIFIED);
            totalHeight += view.getMeasuredHeight();
        }
        ViewGroup.LayoutParams params = listView.getLayoutParams();
        params.height = totalHeight + (listView.getDividerHeight() * (listAdapter.getCount() - 1));
        listView.setLayoutParams(params);
        listView.requestLayout();
    }

    public void onButtonPressed(int posicion, Documento docto, String DoctoIc, String DoctoNom, ArrayList<Documento> documentos) {
        if (mListener != null) {
            mListener.onFragmentInteraction(posicion, docto, DoctoIc, DoctoNom, mInfoTrab.getString(Utilerias.TAG_RESP_FOLIO),
                    mInfoTrab.getString(Utilerias.TAG_RESP_CURP), mInfoTrab.getString(Utilerias.TAG_RESP_FECHA),
                    mInfoTrab.getString(Utilerias.TAG_RESP_TIPO_SOL), documentos);
        }else
            Utilerias.writeLog("Error", "el listener es nulo en onButtonPressed");
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * Turns on activate-on-click mode. When this mode is on, list items will be
     * given the 'activated' state when touched.
     */
    public void setActivateOnItemClick(boolean activateOnItemClick) {
        // When setting CHOICE_MODE_SINGLE, ListView will automatically
        // give items the 'activated' state when touched.
        mLvDoctos.setChoiceMode(
                activateOnItemClick ? ListView.CHOICE_MODE_SINGLE
                        : ListView.CHOICE_MODE_NONE);
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {

        void onFragmentInteraction(int posicion, Documento docto, String DoctoIc, String DoctoNom, String sFolioAfiSer,
                                   String sCurp, String sFecha, String sTipoOp, ArrayList<Documento> documentos);
    }


    private void soapObtenerInfo(String sFolio, String sOpcion) {
        SoapString sSoap = new SoapString(Utilerias.WS_MODULO_MOVIL,
                Utilerias.WS_METODO_OBTENER_INFO_TRAB);
        sSoap.setParams(Utilerias.WS_PARAM_FOLIO, sFolio);
        sSoap.setParams(Utilerias.WS_PARAM_OPCION, sOpcion);
        sSoap.setParams("identificador","1");
        sSoap.setParams("iTabla",iTabla);
        CallWebService callWS =
                new CallWebService(this, Utilerias.SOAP_OBTENER_INFO_TRAB);
        callWS.setFlag(true);
        callWS.execute(sSoap.getSoapString());
    }

    private void soapObtenerDoctos() {
        SoapString sSoap = new SoapString(Utilerias.WS_MODULO_MOVIL,
                Utilerias.WS_METODO_OBTENER_DOCTOS_DIGI);
        sSoap.setParams(Utilerias.WS_PARAM_OPCION, "1");
        sSoap.setParams("iTabla", iTabla);
        sSoap.setParams("cFolio",mFolio);

        Utilerias.writeLog("soapObtenerDoctos",iTabla+"|"+mFolio);

        CallWebService callWS =
                new CallWebService(this, Utilerias.SOAP_OBTENER_DOCTOS_DIGI);
        callWS.setFlag(true);
        callWS.execute(sSoap.getSoapString());
    }

    private void soapObtenerDoctoIndex(String sFolio, String sOpcion, String nomenclatura, int posicionDocto) {
        Utilerias.writeLog("soapObtenerDoctoIndex",sFolio+"|"+sOpcion);
        if(iTabla.equals("8")) {
            SoapString sSoap = new SoapString(Utilerias.WS_MODULO_MOVIL,
                    Utilerias.WS_METODO_OBTENER_DOCTOS_INDEX);

            sSoap.setParams(Utilerias.WS_PARAM_FOLIO, mFolio);
            sSoap.setParams(Utilerias.WS_PARAM_OPCION, sOpcion);
            sSoap.setParams(Utilerias.WS_PARAM_NOMENCLATURA, nomenclatura);
            sSoap.setParams(Utilerias.TAG_RESP_INT_DOC, posicionDocto + "");
            sSoap.setParams("iTabla", iTabla);

            CallWebService callWS =
                    new CallWebService(this, Utilerias.SOAP_OBTENER_DOCTOS_INDEX);
            callWS.setFlag(true);
            callWS.execute(sSoap.getSoapString());
        }
        else
        {
            ipServidor = ipAddress.replace(Utilerias.IP_SERV_MOVIL, Utilerias.IP_SERV_DEV);
            ipServidor = ipServidor.replace(Utilerias.IP_SERV_QA_WS, Utilerias.IP_SERV_QA);
            ipServidor = ipServidor.replace(Utilerias.IP_SERV_QA_WS2, Utilerias.IP_SERV_QA2);
            ipServidor = ipServidor.replace(Utilerias.IP_SERV_PROD_WS, Utilerias.IP_SERV_PROD);
            Utilerias.writeLog("soapObtenerDoctoIndex ipServidor",ipServidor);

            apiInterface = APIClient.getClient(ipServidor).create(APIInterface.class);

            String var = Indexados.createIndexados(sOpcion, mFolio, nomenclatura , iTabla);

            try {
                Call<DoctosIndexados> call = apiInterface.doGetListResourcesIndex(var);

                Utilerias.writeLog(TAG, "doGetListResourcesIndex " + call);
                call.enqueue(new Callback<DoctosIndexados>() {

                    @Override
                    public void onResponse(Call<DoctosIndexados> call, Response<DoctosIndexados> response) {
                        Utilerias.writeLog(TAG, "onResponse ");
                        String xmlAfiliacion;
                        DoctosIndexados resource = response.body();

                        Integer estatus = resource.estatus;
                        String descripcion = resource.descripcion;

                        xmlAfiliacion = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>" +
                                "<SOAP-ENV:Envelope xmlns:SOAP-ENV=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:SOAP-ENC=\"http://schemas.xmlsoap.org/soap/encoding/\" " +
                                "xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:ns2=\"urn:wsafiliacionmovil\">" +
                                "<SOAP-ENV:Header></SOAP-ENV:Header>" +
                                "<SOAP-ENV:Body SOAP-ENV:encodingStyle=\"http://schemas.xmlsoap.org/soap/encoding/\">" +
                                "<ns2:ObtenerDoctosDigitalizarResponse>" +
                                "<outParam>" +
                                "<EstadoProc>" +
                                "<Estatus>200</Estatus>" +
                                "<Respuesta>" + resource.respuesta + "</Respuesta>" +
                                "<Mensaje> </Mensaje>" +
                                "</EstadoProc>" +
                                "<DoctosDigi SOAP-ENC:arrayType=\"ns2:DocumentoDigi[6]\">";

                        sNomenclatura = nomenclatura;
                        iContador = posicionDocto;
                        iResp = Integer.parseInt(resource.respuesta);

                        Utilerias.writeLog(TAG, "soapObtenerInfo iResp " +iResp );

                        //Obtener Tipo Comprobante Domicilio
                        apiObtenerCompDom(mFolio);


                        xmlAfiliacion += "</DoctosDigi>" +
                                "</outParam>" +
                                "</ns2:ObtenerDoctosDigitalizarResponse>" +
                                "</SOAP-ENV:Body>" +
                                "</SOAP-ENV:Envelope>";

                        onTaskComplete(xmlAfiliacion, Utilerias.SOAP_OBTENER_DOCTOS_INDEX);



                    }

                    @Override
                    public void onFailure(Call<DoctosIndexados> call, Throwable t) {
                        call.cancel();
                        Utilerias.writeLog(TAG, "onFailure ");
                    }
                });

            }catch (Exception e){
                Utilerias.writeLog(TAG, "Exception e.toString() " + e.toString());
            }
        }
    }

    private void apiObtenerCompDom(String sFolio) {

        Utilerias.writeLog(TAG, "apiObtenerCompDom sFolio " +sFolio );

        ipServidor = ipAddress.replace(Utilerias.IP_SERV_MOVIL, Utilerias.IP_SERV_DEV);
        ipServidor = ipServidor.replace(Utilerias.IP_SERV_QA_WS, Utilerias.IP_SERV_QA);
        ipServidor = ipServidor.replace(Utilerias.IP_SERV_QA_WS2, Utilerias.IP_SERV_QA2);
        ipServidor = ipServidor.replace(Utilerias.IP_SERV_PROD_WS, Utilerias.IP_SERV_PROD);
        apiInterface = APIClient.getClient(ipServidor).create(APIInterface.class);
        String var = DocComprobante.createDocComprobante(sFolio);

        Utilerias.writeLog("apiObtenerCompDom ipServidor",ipServidor);

        try {
            Call<DoctosCompDom> call = apiInterface.doGetListCompDom(var);
            call.enqueue(new Callback<DoctosCompDom>() {
                @Override
                public void onResponse(Call<DoctosCompDom> call, Response<DoctosCompDom> response) {


                    DoctosCompDom resource = response.body();
                    Integer estatus = resource.estatus;
                    String descripcion = resource.descripcion;
                    String respuesta = resource.respuesta;
                    CompDom = respuesta;
                    Utilerias.writeLog("apiObtenerCompDom CompDom",CompDom);
                }

                @Override
                public void onFailure(Call<DoctosCompDom> call, Throwable t) {
                    call.cancel();
                }
            });

        }catch (Exception e){
            Utilerias.writeLog(TAG, "Exception e.toString() " + e.toString());
        }


    }

    private void soapValidarEstatusProceso(String sCurp, String iEstatusProceso) {
        Utilerias.writeLog("soapValidarEstatusProceso ",sCurp);
        if(iTabla.equals("8")){
            Utilerias.writeLog("soapValidarEstatusProceso ","ENTRO");
            SoapString sSoap = new SoapString(Utilerias.WS_MODULO_MOVIL,
                    Utilerias.WS_METODO_VALIDAR_ESTATUS_PROCESO);
            sSoap.setParams(Utilerias.WS_PARAM_ESTATUS_DIGI, iEstatusProceso);
            sSoap.setParams(Utilerias.WS_PARAM_CURP, sCurp);
            sSoap.setParams("iTabla",iTabla);
            sSoap.setParams("cFolio","1");
            sSoap.setParams("identificador","1");

            Utilerias.writeLog("LOGSSOPA",sSoap.toString());
            CallWebService callWS =
                    new CallWebService(this, Utilerias.SOAP_VALIDAR_ESTATUS_PROCESO);
            callWS.setFlag(true);
            callWS.execute(sSoap.getSoapString());
        }else{

            Utilerias.writeLog("soapValidarEstatusProceso onTaskComplete",sCurp);
            String xmlAfiliacion;

            xmlAfiliacion = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n" +
                    "<SOAP-ENV:Envelope xmlns:SOAP-ENV=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:SOAP-ENC=\"http://schemas.xmlsoap.org/soap/encoding/\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:ns2=\"urn:wsafiliacionmovil\"><SOAP-ENV:Header></SOAP-ENV:Header><SOAP-ENV:Body SOAP-ENV:encodingStyle=\"http://schemas.xmlsoap.org/soap/encoding/\"><ns2:ValidarEstatusProcesoReenrolResponse><outParam><EstadoProc><Estatus>200</Estatus><Mensaje> </Mensaje></EstadoProc><ResultadoActualizar>1</ResultadoActualizar></outParam></ns2:ValidarEstatusProcesoReenrolResponse></SOAP-ENV:Body></SOAP-ENV:Envelope>";

            onTaskComplete(xmlAfiliacion, 730);

        }

    }

    private void soapGuardarLogs(){

        Utilerias.writeLog(TAG, "soapGuardarLogs MensajeLog " +MensajeLog );
        SoapString sSoap = new SoapString("wsafiliacionmovil",
                "GuardarLogs");

        sSoap.setParams("Mensaje", MensajeLog);
        sSoap.setParams("Estatus", EstatusLog);

        CallWebService callWS = new CallWebService(null, 0);
        callWS.setFlag(false);
        callWS.execute(sSoap.getSoapString());
    }

    @Override
    public void onTaskComplete(String xmlMen, int id) {
        if(xmlMen.equals("")){
//            showToast(getActivity(),"Error al procesar la respuesta del servicio");
            Utilerias.writeLog(TAG + " ERROR","Error al procesar la respuesta del servicio xmlMen está vacío");
            Utilerias.showInfoDialog(Objects.requireNonNull(getFragmentManager()),
                    this, Utilerias.DIALOG_TAG_ALERT_WS_FALLO);
            return;
        }
//        HashMap<String, String> values = getValuesXML(xmlMen);
        Bundle values = Utilerias.getBundleValuesXML(xmlMen);
        if(values == null){
            Utilerias.writeLog(TAG + " ERROR", "VALUES ES NULO EN onTaskComplete");
            Utilerias.showInfoDialog(Objects.requireNonNull(getFragmentManager()),
                    this, Utilerias.DIALOG_TAG_ALERT_WS_FALLO);
        }
        //Asigno los estatus a Variables para a continuación removerlos del Bundle
        int iEstatus = Integer.parseInt(Objects.requireNonNull(values).getString(Utilerias.WS_TAG_HTTP_ESTADO));
        String sMensaje = values.getString(Utilerias.WS_TAG_MSG);
        //Remuevo los valores de retorno del HashMap
        values.remove(Utilerias.WS_TAG_HTTP_ESTADO);
        values.remove(Utilerias.WS_TAG_MSG);

        switch(iEstatus){
            case Utilerias.RESP_HTTP_OK:
            case Utilerias.RESP_HTTP_OK_MOD:{
                switch (id){
                    case Utilerias.SOAP_OBTENER_INFO_TRAB: {
                        Utilerias.writeLog("onTaskComplete SOAP_OBTENER_INFO_TRAB ", Utilerias.SOAP_OBTENER_INFO_TRAB + "");
                        //mInfoTrab.putAll(values);
                        //Se adapta el Bundle a las necesidades de la petición
                        mInfoTrab = values;
                        Utilerias.writeLog(TAG, "onTaskComplete mInfoTrab " +mInfoTrab );
                        soapValidarEstatusProceso(mInfoTrab.getString(Utilerias.TAG_RESP_CURP),
                                Utilerias.ESTATUS_PROCESO_ENROL_TERMINADO);
                        adaptarBundle(mFolio);
                        setInfoValues(getView());
                        break;
                    }
                    case Utilerias.SOAP_OBTENER_DOCTOS_DIGI: {
                        Utilerias.writeLog("onTaskComplete SOAP_OBTENER_DOCTOS_DIGI ", Utilerias.SOAP_OBTENER_DOCTOS_DIGI + "");
                        ArrayList<Documento> doctos = values.getParcelableArrayList(Utilerias.TAG_DOCTOS_RETURN);
                        if(iTabla.equals("8")) {
                            soapObtenerDoctoIndex(mFolio, mTipoOperacion, "FARE", 0);
                        }else{
                            for (int i = 0; i < doctos.size(); i++) {
                                soapObtenerDoctoIndex(mFolio, mTipoOperacion, doctos.get(i).getNomenclatura(), i);
                                iContDoc = iContDoc + 1;
                            }
                        }
                        // Create adapter based on items
                        mAdapterDoctos = new DoctoAdapter(getContext(), generarDatosAdapter(doctos));

                        mLvDoctos.setAdapter(mAdapterDoctos);
                        setListViewHeightBasedOnChildren(mLvDoctos);
                        mLvDoctos.setOnItemClickListener((adapterView, item, position, rowId) -> {
                            // Retrieve item based on position
                            final Documento docto = mAdapterDoctos.getItem(position);
                            if(!docto.isbProcesoTerminado() && mEsPrimeraVez){
                                if( Integer.parseInt(CompDom) == 1 && mAdapterDoctos.getItem(position).getNomenclatura().substring(0,2).equals("DO")){
                                    AlertDialog.Builder builder = new AlertDialog.Builder(
                                            Objects.requireNonNull(getActivity()), R.style.AlertDialogCustom
                                    );
                                    builder.setMessage(R.string.msg_Comp_dom)
                                            .setPositiveButton(R.string.btn_Aceptar_Comp_Dom, (dialog, id12) -> {

                                            });
                                    builder.create().show();
                                }else {

                                    mLvDoctos.getChildAt(position).setBackgroundColor(
                                            getResources().getColor(R.color.colorAccent)
                                    );
                                    if (mPosicion != -1) {
                                        mLvDoctos.getChildAt(mPosicion).setBackgroundColor(
                                                getResources().getColor(R.color.colorWhite)
                                        );
                                    }
                                    mPosicion = position;
                                    item.setClickable(false);
                                    item.setFocusable(false);
                                    // Fire selected listener event with item
                                    onButtonPressed(position, docto, mAdapterDoctos.getItem(position).getIcono() + "", mAdapterDoctos.getItem(position).getNomenclatura(), doctos); // <--------------
                                    mEsPrimeraVez = false;
                                }
                            }
                            else if(MainListActivity.mElementoActual == position){
                                Utilerias.writeLog(TAG, "Se presionó el mismo elemento");
                            }
                            else if(!docto.isbProcesoTerminado()){
                                if( Integer.parseInt(CompDom) == 1 && mAdapterDoctos.getItem(position).getNomenclatura().substring(0,2).equals("DO")){
                                    AlertDialog.Builder builder = new AlertDialog.Builder(
                                            Objects.requireNonNull(getActivity()), R.style.AlertDialogCustom
                                    );
                                    builder.setMessage(R.string.msg_Comp_dom)
                                            .setPositiveButton(R.string.btn_Aceptar_Comp_Dom, (dialog, id12) -> {

                                            });
                                    builder.create().show();
                                }else {
                                    AlertDialog.Builder builder = new AlertDialog.Builder(
                                            Objects.requireNonNull(getActivity()), R.style.AlertDialogCustom
                                    );
                                    builder.setMessage(R.string.msg_Cambiar_Pest)
                                            .setPositiveButton(R.string.btn_Aceptar, (dialog, id12) -> {
                                                mLvDoctos.getChildAt(position).setBackgroundColor(
                                                        getResources().getColor(R.color.colorAccent)
                                                );
                                                mLvDoctos.getChildAt(mPosicion).setBackgroundColor(
                                                        getResources().getColor(R.color.colorWhite)
                                                );
                                                mPosicion = position;

                                                item.setSelected(true);
                                                //item.setEnabled(false);
                                                item.setClickable(false);
                                                item.setFocusable(false);
                                                // Fire selected listener event with item
                                                onButtonPressed(position, docto, mAdapterDoctos.getItem(position).getIcono() + "", mAdapterDoctos.getItem(position).getNomenclatura(), doctos); // <--------------
                                            })
                                            .setNegativeButton(R.string.btn_Cancelar, (dialog, id1) ->
                                                    Utilerias.writeLog(TAG, "cancelo el cambio de pestaña"));
                                    builder.create().show();
                                }
                            }else{
                                item.setClickable(false);
                                item.setFocusable(false);
                            }
                        });

                        break;
                    }
                    case Utilerias.SOAP_OBTENER_DOCTOS_INDEX: {
                        Utilerias.writeLog("onTaskComplete SOAP_OBTENER_DOCTOS_INDEX ", Utilerias.SOAP_OBTENER_DOCTOS_INDEX + "");
                        if(iTabla.equals("8")){
                            /*
                            REENROLAMIENTO
                            */
                            int resp = Integer.parseInt(values.getString(Utilerias.TAG_RESP_INT));
                            int pos = Integer.parseInt(values.getString(Utilerias.TAG_RESP_DOC));

                            if(resp == 1){
                                //1 es la posición del FARE el cual es el autoindexado

                                Objects.requireNonNull(mAdapterDoctos.getItem(pos)).
                                        setbProcesoTerminado(true);
                                if(mAdapterDoctos.getItem(pos).getIcono() == R.drawable.ic_file){
                                    Utilerias.cambiarIcono(mLvDoctos.getChildAt(pos), R.drawable.ic_file_check);
                                }
                                else if(mAdapterDoctos.getItem(pos).getIcono() == R.drawable.ic_pic){
                                    Utilerias.cambiarIcono(mLvDoctos.getChildAt(pos), R.drawable.ic_pic_check);
                                }
                                else if(mAdapterDoctos.getItem(pos).getIcono() == R.drawable.ic_ine){
                                    Utilerias.cambiarIcono(mLvDoctos.getChildAt(pos), R.drawable.ic_id_check);
                                }
                                MainListActivity.mNombreDoctos.put(Utilerias.DOCTO_FORMATO_REEN, Utilerias.generarNombreImg(
                                        Utilerias.DOCTO_FORMATO_REEN,
                                        mFolio,
                                        mInfoTrab.getString(Utilerias.TAG_RESP_FECHA),
                                        mInfoTrab.getString(Utilerias.TAG_RESP_CURP),
                                        mInfoTrab.getString(Utilerias.TAG_RESP_TIPO_SOL)
                                ));
                            }else if (resp == 0){
                                Utilerias.writeLog(TAG, "al obtener el documento indexado la respuesta fue 0");
                            }else{
                                Utilerias.writeLog("onTaskComplete SOAP_OBTENER_DOCTOS_INDEX ELSE", Utilerias.SOAP_OBTENER_DOCTOS_INDEX + "");
                                Utilerias.showInfoDialog(Objects.requireNonNull(getFragmentManager()),
                                        this, Utilerias.DIALOG_TAG_ALERT_WS_FALLO);
                            }
                        }else{

                            /*
                            REGISTRO Y TRASPASO DE AFILIADOS
                            */

                            Utilerias.writeLog("onTaskComplete SOAP_OBTENER_DOCTOS_INDEX registro", Utilerias.SOAP_OBTENER_DOCTOS_INDEX + "");

                            int resp = iResp;
                            int pos = iContador;

                            Utilerias.writeLog(TAG, "onTaskComplete resp " +resp );
                            Utilerias.writeLog(TAG, "onTaskComplete pos " +pos );

                            if(resp == 1){

                                Utilerias.writeLog("onTaskComplete SOAP_OBTENER_DOCTOS_INDEX respuesta 1", Utilerias.SOAP_OBTENER_DOCTOS_INDEX + "");

                                incrementarIndexados();

                                if((iContDoc - iDocTerm) == 1 && Integer.parseInt(iTabla) == 1){

                                    MainFragment.mBtnPublicar.setEnabled(true);
                                    MainFragment.mBtnPublicar.setBackgroundColor(getResources().getColor(R.color.colorOk));

                                }

                                Utilerias.writeLog(TAG, "******** " + mAdapterDoctos.getItem(pos).getIcono());
                                //1 es la posición del FARE el cual es el autoindexado
                                Objects.requireNonNull(mAdapterDoctos.getItem(pos)).
                                        setbProcesoTerminado(true);
                                if(mAdapterDoctos.getItem(pos).getIcono() == R.drawable.ic_file){
                                    Utilerias.cambiarIcono(mLvDoctos.getChildAt(pos), R.drawable.ic_file_check);
                                }
                                else if(mAdapterDoctos.getItem(pos).getIcono() == R.drawable.ic_pic){
                                    Utilerias.cambiarIcono(mLvDoctos.getChildAt(pos), R.drawable.ic_pic_check);
                                }
                                else if(mAdapterDoctos.getItem(pos).getIcono() == R.drawable.ic_ine){
                                    Utilerias.cambiarIcono(mLvDoctos.getChildAt(pos), R.drawable.ic_id_check);
                                }

                                if(Integer.parseInt(iTabla) == 1){

                                    Utilerias.cambiarIcono(mLvDoctos.getChildAt(pos), R.drawable.ic_file_check);
                                }

                                MainListActivity.mNombreDoctos.put(Utilerias.DOCTO_FORMATO_EN, Utilerias.generarNombreImg(
                                        Utilerias.DOCTO_FORMATO_EN,
                                        mFolio,
                                        mInfoTrab.getString(Utilerias.TAG_RESP_FECHA),
                                        mInfoTrab.getString(Utilerias.TAG_RESP_CURP),
                                        mInfoTrab.getString(Utilerias.TAG_RESP_TIPO_SOL)
                                ));
                            }else if (resp == 0){

                                Utilerias.writeLog(TAG, "al obtener el documento indexado la respuesta fue 0");

                            }else{
                                Utilerias.showInfoDialog(Objects.requireNonNull(getFragmentManager()),
                                        this, Utilerias.DIALOG_TAG_ALERT_WS_FALLO);
                            }
                        }
                        break;
                    }
                    case Utilerias.SOAP_VALIDAR_ESTATUS_PROCESO: {
                        int resp = Integer.parseInt(values.getString(Utilerias.TAG_RESP_INT));
                        Utilerias.writeLog(TAG, "SOAP_VALIDAR_ESTATUS_PROCESO iTabla " + iTabla);
                        if(resp == 1 || Integer.parseInt(iTabla) == 3){
                            //el proceso está en flujo normal, se generó el fare y se terminó el reenrolamiento
                            //el proceso está en flujo normal, se generó el fare y se terminó el reenrolamiento
                            //tal vez sería bueno un log, pero por ahora lo dejo en todo: log
                            soapObtenerDoctos();
                        }else{
                            Utilerias.writeLog(TAG, "Se cierra porque el estatus no es 3");
                            Utilerias.showInfoDialog(Objects.requireNonNull(getFragmentManager()),
                                    this, Utilerias.DIALOG_TAG_ALERT_ESTATUS_ERR);
                        }
                        break;
                    }
                    default:
                        Utilerias.writeLog(TAG,"HttpResponse: " + iEstatus + " idOp: " + id + ": " +
                                "Esto no debe de pasar en onTaskComplete switch id okWS");
                }
                break;
            }
            case Utilerias.RESP_HTTP_ERR_ACCESO:
            case Utilerias.RESP_HTTP_ERR_NO_ENCONTRADO:
            case Utilerias.RESP_HTTP_ERR_SERVIDOR:
            case Utilerias.RESP_HTTP_ERR_PARAM:{
                MensajeLog = "iEstatus: "+iEstatus+"| id: "+id;
                soapGuardarLogs();
                MensajeLog = "FOLIO: "+mFolio+"| TIPO_OPERACION: "+mTipoOperacion;
                soapGuardarLogs();
                Utilerias.writeLog(TAG,"HttpResponse: " + iEstatus + " idOperación: " + id + ": " + sMensaje);
                switch (id){
                    case Utilerias.SOAP_OBTENER_INFO_TRAB: {
                        Utilerias.writeLog(TAG, "Falló al obtener la info del trabajador");
                        break;
                    }
                    case Utilerias.SOAP_OBTENER_DOCTOS_DIGI: {
                        Utilerias.writeLog(TAG, "Falló al obtener los documentos a digitalizar");
                        break;
                    }
                    case Utilerias.SOAP_OBTENER_DOCTOS_INDEX: {
                        Utilerias.writeLog(TAG, "Falló al obtener los documentos autoindexados");
                        break;
                    }
                    case Utilerias.SOAP_VALIDAR_ESTATUS_PROCESO: {
                        Utilerias.writeLog(TAG, "Falló al validar el estatus del proceso de reenrolamiento");
                        break;
                    }
                    default:
                        Utilerias.writeLog(TAG,"HttpResponse: " + iEstatus + " idOp: " + id + ": " +
                                "Esto no debe de pasar en onTaskComplete switch id errorWS");
                }
                Utilerias.showInfoDialog(Objects.requireNonNull(getFragmentManager()),
                        this, Utilerias.DIALOG_TAG_ALERT_WS_FALLO);
                break;
            }
            default:
                Utilerias.writeLog(TAG,"HttpResponse: " + iEstatus + " idOp: " + id + ": " +
                        "Esto no debe de pasar en onTaskComplete switch wsResponse");
        }

    }


    private ArrayList<Documento> generarDatosAdapter(ArrayList<Documento> doctos) {
        for(int i = 0; i < doctos.size(); i++){
            if(doctos.get(i).getNomenclatura().substring(0,2).equals("FT")){
                doctos.get(i).setIcono(R.drawable.ic_pic);
                doctos.get(i).setPosicion(i);
            }else if(doctos.get(i).getNomenclatura().substring(0,2).equals("ID")){
                doctos.get(i).setIcono(R.drawable.ic_ine);
                doctos.get(i).setPosicion(i);
            }else{
                doctos.get(i).setIcono(R.drawable.ic_file);
                doctos.get(i).setPosicion(i);
            }

        }
        return new ArrayList<>(doctos);
    }

    public static void incrementarIndexados(){

        iDocTerm = iDocTerm +1;

        Utilerias.writeLog(" incrementarIndexados iDocTerm ",iDocTerm + "");
    }

    public static void incrementarCapturados(){
        iDocCap = iDocCap +1;

        Utilerias.writeLog(" incrementarIndexados iDocCap",iDocCap + "");
    }

    private void adaptarBundle(String sFolioAfiSer) {
        String nombres = mInfoTrab.getString(Utilerias.TAG_RESP_NOMBRE);
        assert nombres != null;
        String[] nomDiv = nombres.split(" ");
        mInfoTrab.remove(Utilerias.TAG_RESP_NOMBRE);
        StringBuilder nombre = new StringBuilder();
        for(int i = 0; i < nomDiv.length; i++){
            if(i == nomDiv.length-1){
                nombre.trimToSize();
                mInfoTrab.putString(Utilerias.TAG_RESP_AP_MAT, nomDiv[i]);
            }
            else if (i == nomDiv.length-2)
                mInfoTrab.putString(Utilerias.TAG_RESP_AP_PAT, nomDiv[i]);
            else
                nombre.append(nomDiv[i]).append(" ");
        }
        mInfoTrab.putString(Utilerias.TAG_RESP_NOMBRE, nombre.toString());
        //SE METE EL FOLIO DE AFILIACIÓN O DE SERVICIO AL BUNDLE CON LA INFO DEL TRABAJADOR
        // Y EN CASO DE SER SERVICIO, SE LE AGREGA LA S
        if(mTipoOperacion.equals(Utilerias.OPCION_AFILIACION_TRASPASO))
            mInfoTrab.putString(Utilerias.TAG_RESP_FOLIO, sFolioAfiSer);
        else
            mInfoTrab.putString(Utilerias.TAG_RESP_FOLIO, sFolioAfiSer+"-S");
    }

    private void setInfoValues(View view) {
        final int[] ids = {
                R.id.txt_name,
                R.id.txt_apaterno,
                R.id.txt_amaterno,
                R.id.txt_curp,
                R.id.txt_nss,
                R.id.txt_folio,
                R.id.txt_tipo_sol
        };

        final String[] ARG_PARAMS = new String[]{
                Utilerias.TAG_RESP_NOMBRE,
                Utilerias.TAG_RESP_AP_PAT,
                Utilerias.TAG_RESP_AP_MAT,
                Utilerias.TAG_RESP_CURP,
                Utilerias.TAG_RESP_NSS,
                Utilerias.TAG_RESP_FOLIO,
                Utilerias.TAG_RESP_TIPO_SOL
        };

        for (int i = 0; i < ids.length; i++) {
            TextInputEditText lbl = view.findViewById(ids[i]);
            lbl.setFocusable(false);
            lbl.setText(mInfoTrab.getString(ARG_PARAMS[i]));
        }
    }

    @Override
    public void onDialogPositiveClick(int iTag) {
        int iEstatus = -1;
        switch (iTag){
            case Utilerias.DIALOG_TAG_CANCELAR_DIGI:{
                iEstatus = Utilerias.ERROR_RESPONSE_BTN_CANCEL;
                break;
            }
            case Utilerias.DIALOG_TAG_ALERT_ESTATUS_ERR:{
                iEstatus = Utilerias.ERROR_RESPONSE_WRONG_STATUS;
                break;
            }
            case Utilerias.DIALOG_TAG_ALERT_WS_FALLO:{
                iEstatus = Utilerias.ERROR_RESPONSE_WS_FAILED;
                break;
            }
            case Utilerias.DIALOG_TAG_ALERT_EMPTY_PARAM:{
                iEstatus = Utilerias.ERROR_RESPONSE_EMPTY_PARAMS;
                break;
            }
        }
        Utilerias.terminarAplicacion(Objects.requireNonNull(getActivity()), iEstatus);
    }

    @Override
    public void onDialogNegativeClick(int iTag) {
        switch (iTag){
            case Utilerias.DIALOG_TAG_CANCELAR_DIGI:{
                Utilerias.writeLog(TAG, "cancelo la cancelacion");
                break;
            }
            default:
                Utilerias.writeLog(TAG, "cancelo algo no identificado");
        }
    }
}