package com.aforecoppelafil.my.digitalizadorafilmovil;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.aforecoppelafil.my.digitalizadorafilmovil.documento.Documento;
import com.aforecoppelafil.my.digitalizadorafilmovil.R;

import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.math.BigInteger;
import java.net.NetworkInterface;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Objects;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

public class Utilerias {

    static final String ESTATUS_PROCESO_ENROL_TERMINADO = "3";
    static final String ESTATUS_PROCESO_PUBLICADO       = "2";
    private static final String TIPO_OP_AFILIACION      = "26";
    private static final String TIPO_OP_TRASPASO        = "27";
    private static final String TIPO_OP_INDEPENDIENTE   = "33";
    private static final String CLAVE_OP_DEFAULT        = "990331";
    private static final String CLAVE_OP_AFILIACION     = "000303";
    private static final String CLAVE_OP_TRASPASO       = "000302";
    private static final String CLAVE_OP_INDEPENDIENTE  = "000303";
    private static final String ERROR_CODE_RESPONSE     = "ErrorCode";

    public static final String WS_MODULO_MOVIL = "wsafiliacionmovil";

    /**
     *VALORES DE CONFIGURACIÓN DE LA CÁMARA
     ***/
    public static final int IMG_WIDTH       = 640;
    public static final int IMG_HEIGHT      = 480;
    private static final int DEFAULT_DPI    = 200;

    /**
     * OPCIONES PARA DIFERENCIAR SERVICIOS DE AFILIACIÓN
     */
    static final String OPCION_SERVICIOS = "2";
    static final String OPCION_AFILIACION_TRASPASO = "1";

    /**
     *VALORES DE ENTRADA AL LEVANTAR LA APLICACIÓN
     ***/
    static final String PARAM_NUM_EMPLEADO      = "numEmp";//String
    static final String PARAM_SERVIDOR          = "encoioui";//String
    static final String PARAM_TIPO_OPERACION    = "tipoOperacion";//String
//    static final String PARAM_TABLA_CONSULTA    = "tablaConsulta";//String
    static final String PARAM_FOLIO_SERVICIO    = "folioServicio";//String
    static final String PARAM_FOLIO_AFILIACION  = "folioAfiliacion";//String

    /**
     *IDENTIFICADORES DE ENTRADA AL LEVANTAR LOS FRAGMENT
     ***/
    public static final String PARAM_DOCTO = "docto";
    public static final String PARAM_DOS_PANELES    = "tipoPantalla";
    public static final String PARAM_FOLIO_AFI      = "folioAfi";
    static final String PARAM_DOCTOS_FINALIZADOS    = "doctosFin";
    static final String PARAM_OPCION_TRAMITE    = "OPCION_TRAMITE";//String
//    public static final String PARAM_NOMBRE_DOCTOS  = "nombreDoctos";

    /**
     * RUTAS DOCUMENTOS
     */
    private static final String DIR_LOG     = "Log/";
    public static final String DIR_TMP      = "Temp/";
    public static final String DIR_EXTERNA  = Environment.getExternalStorageDirectory().getAbsolutePath()+"/";
    public static final String DIR_REENROL  = "Afiliacion/";
    static final String DIR_DIGITAL         = "Digitalizador/";
    private static final String NOMBRE_DOC  = "logDigitalizadorMovil" +
            new SimpleDateFormat("dd-MM-yy", Locale.getDefault()).format(new Date()) + ".txt";
    private static final File DIR_LOG_DIGI  =
            new File(DIR_EXTERNA + DIR_REENROL + DIR_LOG + NOMBRE_DOC);

    /**
     *VALORES ESTÁTICOS DE ETIQUETAS DEL WEBSERVICE
     ***/
    private static final String WS_TAG_BASE         = "outParam";
    private static final String WS_TAG_ESTADO       = "EstadoProc";
    public static final String WS_TAG_MSG           = "Mensaje";
    public static final String WS_TAG_HTTP_ESTADO   = "Estatus";

    //    static final String WS_TAG_DOCTOS = "numEmp";

    /**
     *VALORES DE ETIQUETAS DE RESPUESTA
     ***/
    static final String TAG_RESP_NSS        = "Nss";
    static final String TAG_RESP_AP_PAT     = "ApPat";
    static final String TAG_RESP_AP_MAT     = "ApMat";
    static final String TAG_RESP_NOMBRE     = "Nombre";
    static final String TAG_DOCTOS_RETURN   = "doctos";
    static final String TAG_RESP_INT        = "ResultadoActualizar";
    public static final String TAG_RESP_CURP     = "Curp";
    public static final String TAG_RESP_FOLIO    = "Folio";
    public static final String TAG_RESP_FECHA    = "FechaAlta";
    private static final String TAG_OBT_DOCTOS   = "DoctosDigi";
    private static final String TAG_DEFAULT_LBL  = "EMPTY";
    public static final String TAG_RESP_TIPO_SOL = "TipoSolicitud";
    static final String TAG_RESP_INT_DOC    = "iDocPos";
    static final String TAG_RESP_DOC    = "iPosDoc";

    // SERVIDORES
    static final String IP_SERV_MOVIL  = "http://10.27.142.196:50131";
    static final String IP_SERV_DEV  = "http://10.44.172.234";
    static final String IP_SERV_QA_WS = "http://10.27.142.197:50131";
    static final String IP_SERV_QA = "http://10.27.142.242";
    static final String IP_SERV_PROD_WS = "http://10.26.190.158:50131";
    static final String IP_SERV_PROD = "http://10.26.190.175";
    static final String IP_SERV_QA_WS2 = "http://10.27.142.223:50131";
    static final String IP_SERV_QA2 = "http://10.27.142.208";


    /**
    *VALORES DEL FLUJO DEL WEBSERVICE
     */
    static final int SOAP_OBTENER_INFO_TRAB                 = 700;
    static final int SOAP_OBTENER_DOCTOS_DIGI               = 710;
    static final int SOAP_OBTENER_DOCTOS_INDEX              = 720;
    static final int SOAP_VALIDAR_ESTATUS_PROCESO           = 730;
    static final int SOAP_SUBIR_IMG_SERVIDOR                = 740;
    static final int SOAP_SUBIR_IMG_SERVIDOR_IDRE           = 741;
    static final int SOAP_SUBIR_IMG_SERVIDOR_IDRA           = 742;
    static final int SOAP_SUBIR_IMG_SERVIDOR_IDRR           = 743;
    public static final int SOAP_SUBIR_IMG_SERVIDOR_FTRE    = 744;
    static final int SOAP_MOVER_IMGS_INTERMEDIO             = 750;
    static final int SOAP_CONCAT_NUEVAS_NOMENCLATURAS       = 760;
    static final int SOAP_INSERTAR_DOCTOS_DIGI              = 761;
    static final int SOAP_ACTUALIZAR_ESTATUS_COL            = 770;
    static final int SOAP_ACTUALIZAR_ESTATUS_REEN           = 771;
    static final int SOAP_SUBIR_IMG_SERVIDOR_DO00           = 772;

    /**
     * VALORES DE PARAMETROS DEL WEBSERVICE
     */
    static final String WS_PARAM_CURP           ="cCurp";
    static final String WS_PARAM_FOLIO          ="iFolioAfiSer";
    static final String WS_PARAM_OPCION         ="iOpcion";
    static final String WS_PARAM_NOMENCLATURA   ="cNomenclatura";
    static final String WS_PARAM_ESTATUS_DIGI   ="iEstatus";
    static final String WS_PARAM_ES_SERVICIO    ="iEsServicio";
    static final String WS_PARAM_MAC_ADDRES     ="cIpModulo";
    static final String WS_PARAM_NOMBRES_ARCH   ="arrNombreArchivos";
    static final String WS_PARAM_NUM_EMPLEADO   ="iEmpleado";
    static final String WS_PARAM_DOCTOS_INDEXAR ="cDoctosIndexar";
    public static final String WS_PARAM_NOMBRE_IMAGEN   ="cNombreImagen";
    public static final String WS_PARAM_IMAGEN_BASE64   ="sImagen";
    public static final String WS_PARAM_TOKEN_ACCESS    ="tokenAccess";
    public static final String WS_PARAM_TOKEN_APP       ="tokenApp";

    /**
     * NOMBRE DE MÉTODOS DEL WEBSERVICE
     */
    static final String WS_METODO_OBTENER_INFO_TRAB         = "ObtenerDatosTrab";
    static final String WS_METODO_OBTENER_DOCTOS_DIGI       = "ObtenerDoctosDigitalizar";
    static final String WS_METODO_OBTENER_DOCTOS_INDEX      = "ObtenerDocumentoIndexado";
    static final String WS_METODO_VALIDAR_ESTATUS_PROCESO   = "ValidarEstatusProcesoReenrol";
    static final String WS_METODO_ACTUALIZAR_ESTATUS_REEN   = "ActualizarEstatusReenrol";
    static final String WS_METODO_ACTUALIZAR_ESTATUS_COL    = "ActualizarEstatusColImg";
    static final String WS_METODO_INSERTAR_DOCTOS_DIGI      = "InsertarDoctosDigitalizados";
    static final String WS_METODO_CONCAT_NUEVAS_NOMEN       = "ActualizarDoctosDigitalizados";
    static final String WS_METODO_MOVER_IMGS_INTERMEDIO     = "MoverImagenesIntermedio";
    public static final String WS_METODO_SUBIR_IMG_SERVIDOR = "SubirImagenesDigitalizadas";

    /**
     * VALORES DE RESPUESTA DEL WEBSERVICE
     */
    public static final int RESP_HTTP_OK                = 200;
    public static final int RESP_HTTP_OK_MOD            = 201;
    public static final int RESP_HTTP_ERR_PARAM         = 400;
    public static final int RESP_HTTP_ERR_ACCESO        = 403;
    public static final int RESP_HTTP_ERR_SERVIDOR      = 500;
    public static final int RESP_HTTP_ERR_NO_ENCONTRADO = 404;

    /**
     * NOMENCLATURAS DOCTOS
     */
    static final String DOCTO_FORMATO_REEN      = "FARE";
    static final String DOCTO_ID_REEN           = "IDRE";
    static final String DOCTO_ID_REEN_ANV       = "IDRA";
    static final String DOCTO_ID_REEN_REV       = "IDRR";
    public static final String DOCTO_FOTO_REEN  = "FTRE";


    static final String DOCTO_FORMATO_EN      = "FAHD";
    static final String DOCTO_ID_EN           = "ID00";
    static final String DOCTO_ID_EN_ANV       = "ID0A";
    static final String DOCTO_ID_EN_REV       = "ID0R";
    public static final String DOCTO_FOTO_EN  = "FT00";


//    static final String DIALOG_TAG = "dialogTag";

    public static final int DIALOG_TAG_CANCELAR_DIGI = 600;
    public static final int DIALOG_TAG_CONFIRM       = 610;
    public static final int DIALOG_TAG_CONFIRM_RECAP = 611;
    static final int DIALOG_TAG_CONFIRM_RESTART_IDRE = 612;


    static final int DIALOG_TAG_ALERT_CAPTURA               = 100;
    public static final int DIALOG_TAG_ALERT_CAPTURA_FOTO   = 101;
    static final int DIALOG_TAG_ALERT_ESTATUS_ERR           = 110;
    static final int DIALOG_TAG_ALERT_PUBLICAR              = 120;
    public static final int DIALOG_TAG_ALERT_WS_FALLO       = 130;
    static final int DIALOG_TAG_ALERT_FALLO_PUB             = 140;
    static final int DIALOG_TAG_ALERT_NUM_INT_SUP           = 150;
    public static final int DIALOG_TAG_ALERT_EMPTY_PARAM    = 160;
    //public static final int DIALOG_TAG_ALERT_ = 100;

    /**
     * RESPUESTAS DEL INTENT
     */
    public static final int ERROR_RESPONSE_EMPTY_PARAMS  = 800;
    static final int ERROR_RESPONSE_NO_PERMISSIONS       = 810;
    public static final int ERROR_RESPONSE_BTN_CANCEL    = 820;
    static final int ERROR_RESPONSE_WRONG_STATUS         = 830;
    public static final int ERROR_RESPONSE_WS_FAILED     = 840;
    public static final int ERROR_RESPONSE_DIR_NO_EXISTE = 850;

    static final String NAME_REENROLAMIENTO = "Reenrolamiento Móvil";
    static final String NAME_LECTOR_HUELLAS = "EnrollMovil";
    static final String NAME_FIRMA_ENROLA   = "Afil. Firma Digital";
    static final String NAME_DIGITALIZADOR  = "Afil. Digitalizador Móvil";
    static final String NAME_MODULO         = "Menú Módulo Afore";
    static final String NAME_AFILIACION = "Afiliación Trabajador";
    static final String NAME_VIDEO = "Grabador Video";

    static final String PAQREE = "com.aforecoppel.my.reenrolamientomovil";
    static final String PAQDIG = "com.aforecoppelafil.my.digitalizadorafilmovil";
    static final String PAQMOD = "com.aforecoppel.my.modulo";
    static final String PAQFIR = "com.aforecoppel.my.firmadigitalafiliacion";
    static final String PAQAFI = "com.aforecoppel.afiliaciontrabajador";
    static final String PAQVID = "com.android.video";

    /**
     *
     * @param activity Actividad a recibir para levantar el toast
     * @param text Texto que será escrito en el toast
     */

    static void showToast(final Activity activity, final String text) {
        if (activity != null) {
            activity.runOnUiThread(() -> Toast.makeText(activity, text, Toast.LENGTH_SHORT).show());
        }
    }

    public static Bundle getBundleValuesXML(String xmlMen){

        Log.e("Estado", "String xmlMen:" +xmlMen); //respuestaws
        Bundle values = new Bundle();

        try {
            DocumentBuilder newDocumentBuilder =
                    DocumentBuilderFactory.newInstance().newDocumentBuilder();

            Document parse = newDocumentBuilder.parse(
                    new ByteArrayInputStream(xmlMen.getBytes()));

            //LA ETIQUETA OutParam CONTIENE LA INFORMACIÓN QUE REGRESA EL SERVICIO
            NodeList node = parse.getElementsByTagName(WS_TAG_BASE);
            Node elem = node.item(0);
            Node child;
            //String name;

            if(elem != null && elem.hasChildNodes()){
                if(elem.getFirstChild().getNodeName().equals(WS_TAG_ESTADO)) {
                    Node n = parse.getElementsByTagName(WS_TAG_ESTADO).item(0);
                    values.putString(WS_TAG_HTTP_ESTADO, n.getFirstChild().getTextContent());
                    values.putString(WS_TAG_MSG, n.getFirstChild().getNextSibling().getTextContent());

                    for (child = elem.getFirstChild().getNextSibling(); child != null; child = child.getNextSibling()) {
                        values.putString(child.getNodeName(), child.getTextContent());
                    }
                    if (!values.getString(TAG_OBT_DOCTOS, TAG_DEFAULT_LBL).equals(TAG_DEFAULT_LBL)) {
                        ArrayList<Documento> doctos = new ArrayList<>();
                        n = parse.getElementsByTagName(TAG_OBT_DOCTOS).item(0);
                        for (child = n.getFirstChild(); child != null; child = child.getNextSibling()) {
                            //<Identificador>
                            int id = Integer.parseInt(child.getFirstChild().getTextContent());
                            //<IdDocumento>
                            String nomenclatura =
                                    child.getFirstChild().getNextSibling().getTextContent();
                            //<Descripcion>
                            String tipoDocto = child.getFirstChild().getNextSibling().
                                    getNextSibling().getTextContent();
                            doctos.add(new Documento(tipoDocto, id, nomenclatura));
                        }
                        values.putParcelableArrayList(TAG_DOCTOS_RETURN, doctos);
                        values.remove(TAG_OBT_DOCTOS);
                    }
                }else
                    for( child = elem.getFirstChild(); child != null; child = child.getNextSibling() )
                        values.putString(child.getNodeName(), child.getTextContent());
            }

            return values;

        }catch(IOException e){
            e.getStackTrace();
            writeLog("IOException", "ERROR: " + e.getMessage());
            return null;
        } catch(ParserConfigurationException e){
            e.getStackTrace();
            writeLog("ParserConfiguration", "ERROR: " + e.getMessage());
            return null;
        } catch(SAXException e){
            e.getStackTrace();
            writeLog("SAXException", "ERROR: " + e.getMessage());
            return null;
        }
    }

    static String getMacAddr() {
        try {
            List<NetworkInterface> all = Collections.list(NetworkInterface.getNetworkInterfaces());
            for (NetworkInterface nif : all) {
                if (!nif.getName().equalsIgnoreCase("wlan0")) continue;

                byte[] macBytes = nif.getHardwareAddress();
                if (macBytes == null) {
                    return "";
                }

                StringBuilder res1 = new StringBuilder();
                for (byte b : macBytes) {
                    String hex = Integer.toHexString(b & 0xFF);
                    if (hex.length() == 1)
                        hex = "0".concat(hex);
                    res1.append(hex);
                }
                return res1.toString();
            }
        } catch (Exception ex) {
            writeLog("Utilerias", "Error en getMacAddr" + ex.getMessage());
            ex.printStackTrace();
        }
        return "";
    }

    private static String getMacAddrDotted() {
        try {
            List<NetworkInterface> all = Collections.list(NetworkInterface.getNetworkInterfaces());
            for (NetworkInterface nif : all) {
                if (!nif.getName().equalsIgnoreCase("wlan0")) continue;

                byte[] macBytes = nif.getHardwareAddress();
                if (macBytes == null) {
                    return "";
                }

                StringBuilder res1 = new StringBuilder();
                for (byte b : macBytes) {
                    String hex = Integer.toHexString(b & 0xFF);
                    if (hex.length() == 1)
                        hex = "0".concat(hex);
                    res1.append(hex).append(":");
                }
                res1.deleteCharAt(res1.length()-1);
                return res1.toString();
            }
        } catch (Exception ex) {
            writeLog("Utilerias", "Error en getMacAddr" + ex.getMessage());
            ex.printStackTrace();
        }
        return "";
    }

    private static void setDpi(byte[] imageData, int dpi) {
        imageData[13] = 1;
        imageData[14] = (byte) (dpi >> 8);
        imageData[15] = (byte) (dpi & 0xff);
        imageData[16] = (byte) (dpi >> 8);
        imageData[17] = (byte) (dpi & 0xff);
    }

    public static String guardarArchivo(String sNome, String sFolioAfiSer, String sFecha,
                                String sFolioAfi, String sCurp, byte[] imageData, String sTipoOp){

        writeLog("guardarArchivo", "sNome " + sNome);

        String nombre = generarNombreImg(sNome, sFolioAfiSer, sFecha, sCurp, sTipoOp);

        writeLog("generarNombreImg", "nombre " + nombre);

        File file = new File(
                DIR_EXTERNA + DIR_REENROL + sFolioAfi + "/" + DIR_DIGITAL + nombre);

        try (FileOutputStream output = new FileOutputStream(file)) {
            output.write(imageData);
            output.close();
            return nombre;
        } catch (IOException e) {
            writeLog("guardarArchivo", "No existe el directorio especificado " + e.getMessage());
            e.printStackTrace();
            return "";
        }
    }

    static String generarNombreImg(String sNome, String sFolioAfiSer,
                                   String sFecha, String sCurp, String sTipoOp){
        String sClave = CLAVE_OP_DEFAULT;
        switch (sTipoOp){
            case TIPO_OP_AFILIACION:
                sClave = CLAVE_OP_AFILIACION;
                break;
            case TIPO_OP_TRASPASO:
                sClave = CLAVE_OP_TRASPASO;
                break;
            case TIPO_OP_INDEPENDIENTE:
                sClave = CLAVE_OP_INDEPENDIENTE;
                break;
        }
        return sFolioAfiSer + "_" + sCurp + sClave + sFecha + sNome + ".tif";
    }

    public static byte[] tratarImagen(Bitmap img){
        ByteArrayOutputStream imageByteArray = new ByteArrayOutputStream();
        img.compress(Bitmap.CompressFormat.JPEG, 100, imageByteArray);
        byte[] imageData = imageByteArray.toByteArray();

        setDpi(imageData, DEFAULT_DPI);

        return imageData;
    }

    public static void terminarAplicacion(@NonNull Activity activity, int responseCode){
        Intent returnIntent = new Intent();

        returnIntent.putExtra(ERROR_CODE_RESPONSE, responseCode);
        activity.setResult(Activity.RESULT_CANCELED, returnIntent);
        activity.finishAndRemoveTask();
    }

    public static void cambiarIcono(View vElementoLista, int iIconoId) {
        ImageView imgView = vElementoLista.findViewById(R.id.item_icon);
        imgView.setImageResource(iIconoId);
    }

    public static void writeLog(String sTag,String sLog){
        String todayTime =
                new SimpleDateFormat("dd-MM-yy", Locale.getDefault()).format(new Date());
        sLog = todayTime + "[" + sTag + "]" + "|" + sLog + "\n";
        writeFile(sLog.getBytes());

    }

    private static void writeFile(byte[] content){
        try {
            FileOutputStream fos = new FileOutputStream(DIR_LOG_DIGI, true);
            fos.write(content);
            fos.flush();
            fos.close();
            Log.i("writeFile", "El Log se escribió correctamente");
        } catch (Exception e) {
            Log.i("writeFile", "falló en la escritura del Log");
            e.printStackTrace();
        }
    }

    public static String getTokenAcces(String apikey, boolean isTokenAccess){
        String hashHex;
        String cadena = "";
        String mac = getMacAddrDotted();
        String todayDate =
                new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault()).format(new Date());

        if(isTokenAccess) {
            if (!mac.equals(""))
                cadena = apikey + todayDate + mac;
            else
                writeLog("getTokenAcces","Ocurrió un problema al generar el TokenAccess");
        }
        else
            cadena = apikey + todayDate;

        hashHex = bin2hex(getHash(cadena));
        writeLog("getTokenAcces", "se generó el token de acceso exitosamente");
        return hashHex;
    }

    private static byte[] getHash(String password) {
        MessageDigest digest=null;
        try {
            digest = MessageDigest.getInstance("SHA-256");
            writeLog("getHash", "se obtuvo el hash exitosamente");
        } catch (NoSuchAlgorithmException e) {
            writeLog("getHash", "se generó una excepción al tratar de obtener el hash");
            writeLog("getHash", e.getMessage());
            e.printStackTrace();
        }
        digest.reset();
        return digest.digest(password.getBytes());
    }

    private static String bin2hex(byte[] data) {
        return String.format("%0" + (data.length*2) + "x", new BigInteger(1, data));
    }

    public static void showNoticeDialog(@NonNull FragmentManager fragmentManager,
                                        Fragment fragment, int requestCode) {
        // Create an instance of the dialog fragment and show it
        NoticeDialogFragment editNameDialogFragment =
                NoticeDialogFragment.newInstance();
        editNameDialogFragment.setTargetFragment(fragment, requestCode);
        editNameDialogFragment.show(fragmentManager, "weas");
    }

    public static void showInfoDialog(@NonNull FragmentManager fragmentManager,
                                      Fragment fragment, int requestCode) {
        // Create an instance of the dialog fragment and show it

        InfoDialogFragment infoDialogFragment = InfoDialogFragment.newInstance();
        infoDialogFragment.setTargetFragment(fragment, requestCode);
        infoDialogFragment.setCancelable(false);
        infoDialogFragment.show(fragmentManager, "weas");
    }

    public static class NoticeDialogFragment extends DialogFragment {

        // Use this instance of the interface to deliver action events
        NoticeDialogListener mListener;
//        public int miTag;

        /**
         * Constructor vacío requerido
         */
        public NoticeDialogFragment() {
        }

        /**
         * Constructor para crear el fragment que recibe un bundle como parametro
         * @return NoticeDialogFragment
         */
        public static NoticeDialogFragment newInstance() {
            return new NoticeDialogFragment();
        }

        /** The activity that creates an instance of this dialog fragment must
         * implement this interface in order to receive event callbacks.
         * Each method passes the DialogFragment in case the host needs to query it.
         */
        public interface NoticeDialogListener {
            void onDialogPositiveClick(int iTag);
            void onDialogNegativeClick(int iTag);

        }

        @Override
        public void onCreate(@Nullable Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            setStyle(DialogFragment.STYLE_NO_TITLE, R.style.AlertDialogCustom);
        }

        @NonNull
        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            int iMensaje = R.string.dummy_content;
            switch(getTargetRequestCode()){
                case DIALOG_TAG_CANCELAR_DIGI:{
                    iMensaje = R.string.msg_Cancelar_Digi;
                    break;
                }
                case DIALOG_TAG_CONFIRM:{
                    iMensaje = R.string.msg_Confirmar_Img;
                    break;
                }
                case DIALOG_TAG_CONFIRM_RECAP:{
                    iMensaje = R.string.msg_Confirmar_Recap;
                    break;
                }
                case DIALOG_TAG_CONFIRM_RESTART_IDRE:{
                    iMensaje = R.string.msg_Confirmar_Restart_IDRE;
                    break;
                }
                default:
                    writeLog("NoticeDialog","requestCode es del diablo");
            }
            // Build the dialog and set up the button click handlers
            AlertDialog.Builder builder = new AlertDialog.Builder(
                    Objects.requireNonNull(getActivity()), R.style.AlertDialogCustom
            );
            builder.setMessage(iMensaje).setPositiveButton(R.string.btn_Aceptar, (dialog, id) -> {
                        // Send the positive button event back to the host activity
                        mListener.onDialogPositiveClick(getTargetRequestCode());
                        dismiss();
                    }).setNegativeButton(R.string.btn_Cancelar, (dialog, id) -> {
                        // Send the negative button event back to the host activity
                        mListener.onDialogNegativeClick(getTargetRequestCode());
                        dismiss();
                    });
            return builder.create();
        }

        // Override the Fragment.onAttach() method to instantiate the NoticeDialogListener
        @Override
        public void onAttach(Context context) {
            super.onAttach(context);
            // Verify that the host activity implements the callback interface
            try {
                // Instantiate the NoticeDialogListener so we can send events to the host
                mListener = (NoticeDialogListener) getTargetFragment();
            } catch (ClassCastException e) {
                // The activity doesn't implement the interface, throw exception
                assert getTargetFragment() != null;
                throw new ClassCastException(getTargetFragment().toString()
                        + " must implement NoticeDialogListener");
            }
        }

    }//NoticeDialogFragment

    public static class InfoDialogFragment extends DialogFragment {

        // Use this instance of the interface to deliver action events
//        NoticeDialogListener mListener;
//        public int miTag;
        NoticeDialogListener mListener;

        public interface NoticeDialogListener {
            void onDialogPositiveClick(int iTag);
        }

        /**
         * Constructor vacío requerido
         */
        public InfoDialogFragment() {
        }

        /**
         * Constructor para crear el fragment que recibe un bundle como parametro
         * @return InfoDialogFragment
         */
        public static InfoDialogFragment newInstance() {
            return new InfoDialogFragment();
        }

        /** The activity that creates an instance of this dialog fragment must
         * implement this interface in order to receive event callbacks.
         * Each method passes the DialogFragment in case the host needs to query it.
         */
        /*public interface NoticeDialogListener {
            public void onDialogPositiveClick(int iTag);
            public void onDialogNegativeClick(int iTag);

        }*/

        @Override
        public void onCreate(@Nullable Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            setStyle(DialogFragment.STYLE_NO_TITLE, R.style.AlertDialogCustom);
        }

        @NonNull
        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            int iMensaje = R.string.dummy_content;
            switch(getTargetRequestCode()){
                case DIALOG_TAG_ALERT_CAPTURA:{
                    iMensaje = R.string.msg_Alert_Captura;
                    break;
                }
                case DIALOG_TAG_ALERT_ESTATUS_ERR:{
                    iMensaje = R.string.msg_Estatus_Incorrecto;
                    break;
                }
                case DIALOG_TAG_ALERT_PUBLICAR:{
                    iMensaje = R.string.msg_Publicar;
                    break;
                }
                case DIALOG_TAG_ALERT_WS_FALLO:{
                    iMensaje = R.string.msg_Error_WebService;
                    break;
                }
                case DIALOG_TAG_ALERT_CAPTURA_FOTO:{
                    iMensaje = R.string.msg_Alert_Captura_Foto;
                    break;
                }
                case DIALOG_TAG_ALERT_FALLO_PUB:{
                    iMensaje = R.string.msg_Alert_Fallo_Publicar;
                    break;
                }
                case DIALOG_TAG_ALERT_NUM_INT_SUP:{
                    iMensaje = R.string.msg_Alert_Publicar_Exedido;
                    break;
                }
                case DIALOG_TAG_ALERT_EMPTY_PARAM:{
                    iMensaje = R.string.msg_Param_Vacios_Fragment;
                    break;
                }
                default:
                    writeLog("InfoDialog", "requestCode es del diablo");
            }
            // Build the dialog and set up the button click handlers
            AlertDialog.Builder builder = new AlertDialog.Builder(
                    Objects.requireNonNull(getActivity()), R.style.AlertDialogCustom
            );
            builder.setMessage(iMensaje)
                    .setPositiveButton(R.string.btn_Continuar, (dialog, id) -> {
                        // Send the positive button event back to the host activity
                        Log.e("InfoDialogFragment", "Se presionó le btn Aceptar");
                        mListener.onDialogPositiveClick(getTargetRequestCode());
                        dismiss();
                    });
            AlertDialog alertDialog = builder.create();
            alertDialog.setCanceledOnTouchOutside(false);
            return alertDialog;
        }

        @Override
        public void onAttach(Context context) {
            super.onAttach(context);
            // Verify that the host activity implements the callback interface
            try {
                // Instantiate the NoticeDialogListener so we can send events to the host
                mListener = (NoticeDialogListener) getTargetFragment();
            } catch (ClassCastException e) {
                // The activity doesn't implement the interface, throw exception
                assert getTargetFragment() != null;
                throw new ClassCastException(getTargetFragment().toString()
                        + " must implement NoticeDialogListener");
            }
        }

    }//InfoDialogFragment

     /*static HashMap<String, String> getHashMapValuesXML(String xmlMen){

        HashMap<String, String> values = new HashMap<>();

        try {
            DocumentBuilder newDocumentBuilder =
                    DocumentBuilderFactory.newInstance().newDocumentBuilder();

            Document parse = newDocumentBuilder.parse(
                    new ByteArrayInputStream(xmlMen.getBytes()));

            //LA ETIQUETA OutParam CONTIENE LA INFORMACIÓN QUE REGRESA EL SERVICIO
            NodeList node = parse.getElementsByTagName("outParam");
            Node elem = node.item(0);

            Node child;
            String name;

            if(elem != null && elem.hasChildNodes()){
                for( child = elem.getFirstChild(); child != null; child = child.getNextSibling() ){
                    name = child.getNodeName();
                    if(name.equals("EstadoProc")){
                        Node n = parse.getElementsByTagName("EstadoProc").item(0);
                        values.put(WS_TAG_HTTP_ESTADO, n.getFirstChild().getTextContent());
                        values.put(WS_TAG_MSG, n.getFirstChild().getNextSibling().getTextContent());
                    }
                    else{
                        values.put(child.getNodeName(), child.getTextContent());
                    }
                }
            }

            return values;

        }catch(IOException e){
            e.getStackTrace();
            Log.e("IOException","ERROR: " + e.getMessage());
            return null;
        } catch(ParserConfigurationException e){
            e.getStackTrace();
            Log.e("ParserConfiguration","ERROR: " + e.getMessage());
            return null;
        } catch(SAXException e){
            e.getStackTrace();
            Log.e("SAXException","ERROR: " + e.getMessage());
            return null;
        }
    }*/
}
