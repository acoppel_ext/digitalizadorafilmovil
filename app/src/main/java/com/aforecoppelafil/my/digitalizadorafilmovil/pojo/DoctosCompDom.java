package com.aforecoppelafil.my.digitalizadorafilmovil.pojo;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Sergio Gil (AP Interfaces) on 14/12/18.
 */

public class DoctosCompDom {

    @SerializedName("estatus")
    public Integer estatus;
    @SerializedName("descripcion")
    public String descripcion;
    @SerializedName("respuesta")
    public String respuesta;
}
