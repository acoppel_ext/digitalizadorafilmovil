package com.aforecoppelafil.my.digitalizadorafilmovil;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.FrameLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.aforecoppelafil.my.digitalizadorafilmovil.camara.CameraDetailDocFragment;
import com.aforecoppelafil.my.digitalizadorafilmovil.camara.CameraDetailFragment;
import com.aforecoppelafil.my.digitalizadorafilmovil.documento.Documento;
import com.aforecoppelafil.my.digitalizadorafilmovil.pojo.DocComprobante;
import com.aforecoppelafil.my.digitalizadorafilmovil.pojo.DoctosCompDom;
import com.aforecoppelafil.my.digitalizadorafilmovil.webservice.CallWebService;
import com.aforecoppelafil.my.digitalizadorafilmovil.R;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Objects;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.aforecoppelafil.my.digitalizadorafilmovil.DoctoListFragment.mAdapterDoctos;

public class MainListActivity extends AppCompatActivity
        implements DoctoListFragment.OnFragmentInteractionListener,
        DoctoDetailFragment.OnFragmentInteractionListener,
        CameraDetailFragment.OnFragmentInteractionListener,
        CameraDetailDocFragment.OnFragmentInteractionListener
{

    private static final String TAG = "MainListActivity";
    private static final int PERMISSIONS_REQUEST = 999;
    private boolean mIsTwoPane = false;
    static HashMap<String,String> mNombreDoctos;
    public static ArrayList<String> mDoctosNoSubidos;
    private String mFolioAfi;
    public String ipAddress;
    private String mFolioSer;
    private String mNumEmpleado;
    private Integer iOpcionTramite=1;
    private String mCurp;
    private boolean mEsServicio;
    public static int mElementoActual;
    private TextView tvVersionD;
    private Spinner spinnerD;
    private ArrayList<Documento> Documentos;


    public String CompDom = "0";
    public String ipServidor;
    public Integer iTabla = 0;
    APIInterface apiInterface;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_list);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
        Bundle info;
        if(getIntent().hasExtra(Utilerias.PARAM_SERVIDOR)) {
            info = getIntent().getExtras();
            revisarPermisos();
            if(info != null) {
                //Creo una instancia para asignarle la variable al webService
                CallWebService callWebService = new CallWebService(info.getString(Utilerias.PARAM_SERVIDOR));
                ipAddress       = info.getString(Utilerias.PARAM_SERVIDOR);
                mNumEmpleado    = info.getString(Utilerias.PARAM_NUM_EMPLEADO);
                mFolioAfi       = info.getString(Utilerias.PARAM_FOLIO_AFILIACION);
                mEsServicio     = info.getString(Utilerias.PARAM_TIPO_OPERACION).equals(Utilerias.OPCION_SERVICIOS);
                iTabla          = info.getInt("OPCION_TRAMITE");
                mFolioSer = "0";
                iOpcionTramite = info.getInt(Utilerias.PARAM_OPCION_TRAMITE);

                info.putString(Utilerias.PARAM_OPCION_TRAMITE,iOpcionTramite+"");
                info.putString("OPCION_TRAMITE",iTabla+"");
                if (mEsServicio)
                    mFolioSer = info.getString(Utilerias.PARAM_FOLIO_SERVICIO);

                if(iOpcionTramite == 3){
                    apiObtenerCompDom(mFolioAfi);
                }

            }
            tvVersionD = findViewById(R.id.tvVersionD);
            tvVersionD.setText("V "+ BuildConfig.VERSION_NAME);
            spinnerD = findViewById(R.id.spinner);
            mNombreDoctos = new HashMap<>();
            mDoctosNoSubidos = new ArrayList<>();
            ObtenerVersionesApks();
            DoctoListFragment fragmentItem = DoctoListFragment.newInstance(info);
            FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
            ft.replace(R.id.fragmentDoctoList, fragmentItem);
            ft.commit();
        }
        else{
            Utilerias.writeLog(TAG, "El programa se levantó sin parámetros");
            /**
             * Se muestra mensaje de que no puede ser levantado sin parámetros
             */
            AlertDialog.Builder builder = new AlertDialog.Builder(
                    this, R.style.AlertDialogCustom
            );
            builder.setMessage(R.string.msg_Param_Vacios)
                    .setPositiveButton(R.string.btn_Aceptar, (dialog, id) ->
                            Utilerias.terminarAplicacion(MainListActivity.this, Utilerias.ERROR_RESPONSE_EMPTY_PARAMS));
            AlertDialog alert = builder.create();
            alert.setCanceledOnTouchOutside(false);
            alert.setCancelable(false);
            alert.show();

            //info = new Bundle();
            /**
             * Parámetros de pruebas Servicio
             */
            /*info.putString(PARAM_TIPO_OPERACION, "2");
            info.putString(PARAM_TABLA_CONSULTA, "8");
            info.putString(PARAM_FOLIO_AFILIACION, "2459810");
            info.putString(PARAM_FOLIO_SERVICIO, "21364282");
            info.putString(PARAM_NUM_EMPLEADO, "98104985");*/
            /**
             * Parámetros de pruebas Afiliación
             */
            /*info.putString(PARAM_TIPO_OPERACION, "1");
            info.putString(PARAM_TABLA_CONSULTA, "3");
            info.putString(PARAM_FOLIO_AFILIACION, "16212342");
            info.putString(PARAM_FOLIO_SERVICIO, "0");
            info.putString(PARAM_NUM_EMPLEADO, "98104985");*/

            /**
             * Parámetros Lineas a ejecutar para pruebas
             */
            /*info.putString(PARAM_SERVIDOR, "http://10.27.142.196:20307");
            revisarPermisos();
            CallWebService callWebService = new CallWebService(info.getString(PARAM_SERVIDOR));
            mFolioAfi = Objects.requireNonNull(info).getString(PARAM_FOLIO_AFILIACION);
            mFolioSer = Objects.requireNonNull(info).getString(PARAM_FOLIO_SERVICIO);
            mEsServicio = Objects.requireNonNull(Objects.requireNonNull(info).
                    getString(PARAM_TIPO_OPERACION)).equals(OPCION_SERVICIOS);
            mNumEmpleado = Objects.requireNonNull(info).getString(PARAM_NUM_EMPLEADO);

            mNombreDoctos = new HashMap<>();
            mDoctosNoSubidos = new ArrayList<>();

            DoctoListFragment fragmentItem = DoctoListFragment.newInstance(info);
            FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
            ft.replace(R.id.fragmentDoctoList, fragmentItem);
            ft.commit();*/
        }
//            for (String key: info.keySet())
    }

    public void ObtenerVersionesApks(){
        PackageInfo pinfo;
        String[] packapks = {Utilerias.PAQMOD, Utilerias.PAQAFI, Utilerias.PAQFIR, Utilerias.PAQDIG};
        String[] NombresApks = {Utilerias.NAME_MODULO+"\t", Utilerias.NAME_AFILIACION, Utilerias.NAME_FIRMA_ENROLA+"\t\t\t\t\t\t", Utilerias.NAME_DIGITALIZADOR+"\t\t"};
        PackageManager pm = getBaseContext().getPackageManager();
        ArrayList<String> arraySpinner = new ArrayList<String>();

        for (int i = 0; i < packapks.length; i++){
            try {
                if(isPackageInstalled(packapks[i], pm)) {
                    pinfo = getPackageManager().getPackageInfo(packapks[i], 0);
                    arraySpinner.add(NombresApks[i] + "\t" + pinfo.versionName);
                }
            } catch (PackageManager.NameNotFoundException e) {
                Utilerias.writeLog("","VERSIONES: "+e.getMessage());
            }
        }

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, arraySpinner);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerD.setAdapter(adapter);
    }

    private boolean isPackageInstalled(String packageName, PackageManager packageManager) {
        try {
            packageManager.getPackageInfo(packageName, 0);
            return true;
        } catch (PackageManager.NameNotFoundException e) {
            return false;
        }
    }

    @Override
    protected void onPostCreate(@Nullable Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        determinePaneLayout();

        ArrayList<Documento> doctos = new ArrayList<Documento>();

        Bundle info = new Bundle();
        info.putBoolean(Utilerias.PARAM_DOS_PANELES, mIsTwoPane);
        info.putBundle(Utilerias.PARAM_DOCTOS_FINALIZADOS, new Bundle());
        info.putString(Utilerias.PARAM_SERVIDOR, ipAddress);
        info.putBoolean("OPCION_DOCTOS", false);
        info.putParcelableArrayList("ArDoctos", doctos);
        info.putString(Utilerias.PARAM_FOLIO_AFI, mFolioAfi);
        info.putString("comprobante_domicilio", CompDom);
        info.putString(Utilerias.PARAM_OPCION_TRAMITE,iOpcionTramite+"");
        MainFragment fragmentItem2 = MainFragment.newInstance(info);
        FragmentTransaction ft2 = getSupportFragmentManager().beginTransaction();
        ft2.replace(R.id.flDetailContainer, fragmentItem2);
        ft2.commit();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        android.os.Process.killProcess(android.os.Process.myPid());
        System.exit(1);
    }

    private void determinePaneLayout() {
        FrameLayout fragmentDoctoDetail = findViewById(R.id.flDetailContainer);
        // If there is a second pane for details
        if (fragmentDoctoDetail != null) {
            mIsTwoPane = true;
            DoctoListFragment fragmentDoctoList = (DoctoListFragment) getSupportFragmentManager().
                    findFragmentById(R.id.fragmentDoctoList);
            if (fragmentDoctoList == null)
                Utilerias.writeLog("ERROR", "ewe, el frag es nulo :c");
            else
                fragmentDoctoList.setActivateOnItemClick(true);
        }
    }

    @Override
    public void onFragmentInteraction(int posicion, Documento docto, String DoctoIc, String DoctoNom, String sFolioAfiSer,
                                      String sCurp, String sFecha, String sTipoOp, ArrayList<Documento> documentos) {
        mCurp = sCurp;
        mElementoActual = posicion;
        Bundle params = new Bundle();
        params.putString(Utilerias.TAG_RESP_CURP, sCurp);
        params.putString(Utilerias.TAG_RESP_FECHA, sFecha);
        params.putString(Utilerias.TAG_RESP_TIPO_SOL, sTipoOp);
        params.putString(Utilerias.PARAM_SERVIDOR, ipAddress);
        params.putString(Utilerias.PARAM_FOLIO_AFI, mFolioAfi);
        params.putString(Utilerias.TAG_RESP_FOLIO, sFolioAfiSer);
        params.putString("comprobante_domicilio", CompDom);
        params.putString(Utilerias.PARAM_OPCION_TRAMITE , iOpcionTramite+"");
        params.putBoolean(Utilerias.PARAM_DOS_PANELES, mIsTwoPane);
        params.putParcelable(Utilerias.PARAM_DOCTO, docto);
        Documentos = documentos;

        if (mIsTwoPane) { // single activity with list and detail
            if(docto.getNomenclatura().equals(Utilerias.DOCTO_ID_REEN) || docto.getNomenclatura().substring(0,2).equals("ID")){
                // Replace frame layout with correct detail fragment
                DoctoDetailFragment fragmentItem = DoctoDetailFragment.newInstance(params);
                FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
                ft.replace(R.id.flDetailContainer, fragmentItem);
                ft.commit();
            }else if (docto.getNomenclatura().equals(Utilerias.DOCTO_FOTO_REEN) || docto.getNomenclatura().substring(0,2).equals("FT")){

                CameraDetailFragment fragmentCam = CameraDetailFragment.newInstance(params);
                FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
                ft.replace(R.id.flDetailContainer, fragmentCam);
                ft.commit();
            }else{
                CameraDetailDocFragment fragmentCams = CameraDetailDocFragment.newInstance(params);
                FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
                ft.replace(R.id.flDetailContainer, fragmentCams);
                ft.commit();
            }
        }else {
            // For phone, launch detail activity using intent
            Intent i = new Intent(this, MainDetailActivity.class);
            // Embed the serialized item
            i.putExtras(params);
            // Start the activity
            startActivity(i);
            //TODO: HACER CASO EN EL QUE SE LEVANTA LA CAMARA
        }

    }

    @Override
    public void onFragmentInteraction(HashMap<String,String> nombresDoctos, boolean isTwoPane) {
        mNombreDoctos.putAll(nombresDoctos);
        Objects.requireNonNull(mAdapterDoctos.getItem(mElementoActual)).setbProcesoTerminado(true);

        Bundle bDoctoFinalizado = new Bundle();
        for(int i = 0; i < mAdapterDoctos.getCount(); i++){
            Documento docto = mAdapterDoctos.getItem(i);
            assert docto != null;
            bDoctoFinalizado.putBoolean(docto.getNomenclatura(), docto.isbProcesoTerminado());
        }
        /*for(String nom : mNombreDcotos){
            writeLog();(TAG, nom);
        }*/
        Bundle info = new Bundle();
        info.putString(Utilerias.TAG_RESP_CURP, mCurp);
        info.putString(Utilerias.PARAM_FOLIO_AFI, mFolioAfi);
        info.putString(Utilerias.PARAM_SERVIDOR, ipAddress);
        info.putString(Utilerias.PARAM_FOLIO_SERVICIO, mFolioSer);
        info.putString(Utilerias.PARAM_NUM_EMPLEADO, mNumEmpleado);
        info.putString("comprobante_domicilio", CompDom);
        info.putBundle(Utilerias.PARAM_DOCTOS_FINALIZADOS, bDoctoFinalizado);
        info.putBoolean(Utilerias.PARAM_DOS_PANELES, isTwoPane);
        info.putBoolean(Utilerias.WS_PARAM_ES_SERVICIO, mEsServicio);
        info.putString(Utilerias.PARAM_OPCION_TRAMITE,iOpcionTramite+"");
        info.putBoolean("OPCION_DOCTOS",true);
        info.putParcelableArrayList("ArDoctos",Documentos);

        MainFragment fragmentItem2 = MainFragment.newInstance(info);
        FragmentTransaction ft2 = getSupportFragmentManager().beginTransaction();
        ft2.replace(R.id.flDetailContainer, fragmentItem2);
        ft2.commit();
    }

    private void revisarPermisos() {
        // Here, thisActivity is the current activity
        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED ||
                ContextCompat.checkSelfPermission(this,
                        Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {

            ActivityCompat.requestPermissions(this, new String[]{
                    Manifest.permission.WRITE_EXTERNAL_STORAGE,
                    Manifest.permission.CAMERA
            }, PERMISSIONS_REQUEST);

        }
    }

    private void apiObtenerCompDom(String sFolio) {

        Utilerias.writeLog(TAG, "apiObtenerCompDom " + sFolio);
        ipServidor = ipAddress.replace(Utilerias.IP_SERV_MOVIL, Utilerias.IP_SERV_DEV);
        ipServidor = ipServidor.replace(Utilerias.IP_SERV_QA_WS, Utilerias.IP_SERV_QA);
        ipServidor = ipServidor.replace(Utilerias.IP_SERV_QA_WS2, Utilerias.IP_SERV_QA2);
        ipServidor = ipServidor.replace(Utilerias.IP_SERV_PROD_WS, Utilerias.IP_SERV_PROD);
        apiInterface = APIClient.getClient(ipServidor).create(APIInterface.class);
        String var = DocComprobante.createDocComprobante(sFolio);

        try {
            Call<DoctosCompDom> call = apiInterface.doGetListCompDom(var);
            call.enqueue(new Callback<DoctosCompDom>() {
                @Override
                public void onResponse(Call<DoctosCompDom> call, Response<DoctosCompDom> response) {

                    DoctosCompDom resource = response.body();
                    Integer estatus = resource.estatus;
                    String descripcion = resource.descripcion;
                    String respuesta = resource.respuesta;
                    CompDom = respuesta;

                }

                @Override
                public void onFailure(Call<DoctosCompDom> call, Throwable t) {
                    call.cancel();
                }
            });

        }catch (Exception e){
            Utilerias.writeLog(TAG, "Exception e.toString() " + e.toString());
        }

    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String permissions[],
                                           @NonNull int[] grantResults) {
        switch (requestCode) {
            case PERMISSIONS_REQUEST: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    Utilerias.writeLog(TAG, "Los permisos SÍ fueron otorgados ");

                } else {
                    Utilerias.writeLog(TAG, "Los permisos NO fueron otorgados");
                    AlertDialog.Builder builder = new AlertDialog.Builder(
                            this, R.style.AlertDialogCustom
                    );
                    builder.setMessage(R.string.msg_Permisos).setPositiveButton(
                            R.string.btn_Aceptar, (dialog, id) ->
                                    Utilerias.terminarAplicacion(MainListActivity.this,
                                            Utilerias.ERROR_RESPONSE_NO_PERMISSIONS)
                    );
                    AlertDialog alert = builder.create();
                    alert.setCanceledOnTouchOutside(false);
                    alert.setCancelable(false);
                    alert.show();
                }
            }
        }
    }

    @Override
    public void onBackPressed() {
        AlertDialog.Builder builder = new AlertDialog.Builder(
                this, R.style.AlertDialogCustom
        );
        builder.setMessage(R.string.msg_Cancelar_Digi)
                .setPositiveButton(R.string.btn_Aceptar, (dialog, id) -> {
                    Utilerias.writeLog(TAG, "Se presionó botón aceptar después de presionar el btn back");
                    Utilerias.terminarAplicacion(MainListActivity.this, Utilerias.ERROR_RESPONSE_BTN_CANCEL);
                })
                .setNegativeButton(R.string.btn_Cancelar, (dialog, id) ->
                    Utilerias.writeLog(TAG, "Se presionó botón cancelar después de presionar el btn back"));
        AlertDialog alert = builder.create();
        alert.setCanceledOnTouchOutside(false);
        alert.setCancelable(false);
        alert.show();
    }
}
