package com.aforecoppelafil.my.digitalizadorafilmovil.documento;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.aforecoppelafil.my.digitalizadorafilmovil.R;

import java.util.ArrayList;

public class DoctoAdapter extends ArrayAdapter<Documento> {

private final Context context;
private final ArrayList<Documento> modelsArrayList;

    public DoctoAdapter(Context context, ArrayList<Documento> modelsArrayList) {
        super(context, R.layout.target_docto, modelsArrayList);
        this.context = context;
        this.modelsArrayList = modelsArrayList;
    }

    @NonNull
    @Override
    public View getView(int position, View convertView, @NonNull ViewGroup parent) {
        // 1. Create inflater
        LayoutInflater inflater = (LayoutInflater) context
        .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        // 2. Get rowView from inflater
        View rowView;

        rowView = inflater.inflate(R.layout.target_docto, parent, false);

        // 3. Get icon,title & counter views from the rowView
        ImageView imgView = rowView.findViewById(R.id.item_icon);
        TextView titleView = rowView.findViewById(R.id.item_title);

        // 4. Set the text for textView
        imgView.setImageResource(modelsArrayList.get(position).getIcono());
        titleView.setText(modelsArrayList.get(position).toString());
        //titleView.setGravity(Gravity.CENTER);
        //rowView.setSelected(true);

        // 5. return rowView
        return rowView;
    }

}
